package net.pvp_hub.plugin.Infectionwars.lobby;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.pvp_hub.core.PvPHubCore;
import net.pvp_hub.core.api.GameState;
import net.pvp_hub.plugin.Infectionwars.game.Game;
import net.pvp_hub.plugin.Infectionwars.general.ClassSelector;
import net.pvp_hub.plugin.Infectionwars.general.Map;
import net.pvp_hub.plugin.Infectionwars.general.MapVote;
import net.pvp_hub.plugin.Infectionwars.general.PlayerScoreboard;
import net.pvp_hub.plugin.Infectionwars.main.InfectionWars;

public class Lobby {
	private InfectionWars If;
	private World lobbyWorld;
	private Location lobbySpawn;
	private LobbyListener lobbylistener;
	private ItemStack[] LobbyItems;
	private PlayerScoreboard OnlinePlayerBoard;
	private int ScoreboardUpdaterTaskID;
	private ClassSelector selector;
	private ItemStack MapVoter;
	private ItemStack ClassSelector;
	private MapVote vote;
	private int CountDown = 120;
	private int InitRoundTaskID;

	public Lobby(InfectionWars If, World LobbyWorld) {
		System.gc();
		PvPHubCore.setGameState(GameState.LOBBY);
		this.If = If;
		this.lobbyWorld = LobbyWorld;
		this.lobbySpawn = LobbyWorld.getSpawnLocation();
		if (If.getGame() != null) {
			If.getGame().getGameListener().unregisterGameListener();
			If.setGame(null);
		}
		this.lobbylistener = new LobbyListener(If);
		this.OnlinePlayerBoard = new PlayerScoreboard(If, 1);
		this.selector = new ClassSelector(If);
		vote = new MapVote(If);
		createItems();
		initArrays();
		ItemStack[] i = new ItemStack[4];
		for (Player p : If.getServer().getOnlinePlayers()) {
			for (Player ps : If.getServer().getOnlinePlayers()) {
				if (!ps.equals(p)) {
					p.showPlayer(ps);
				}
			}
			p.getInventory().setArmorContents(i);
			p.teleport(lobbySpawn);
			p.getInventory().clear();
			p.setGameMode(GameMode.ADVENTURE);
			addLobbyItems(p);
		}
		killAllMonsters();
		createTasks();
	}

	private void killAllMonsters(){
		for(Entity e : this.lobbyWorld.getEntities()){
			if(!e.isDead() && !(e instanceof Player)){
				e.remove();
			}
		}
	}
	
	private void initArrays() {
		LobbyItems = new ItemStack[10];
		LobbyItems[0] = MapVoter;
	}

	public void addLobbyItems(Player p) {
		p.getInventory().setContents(LobbyItems);
	}

	/**
	 * Updates Level with count of online players
	 */
	private void createTasks() {
		ScoreboardUpdaterTaskID = If.getServer().getScheduler()
				.scheduleSyncRepeatingTask(If, new Runnable() {

					public void run() {
						if (CountDown == 120 || CountDown == 0) {
							for (Player p : If.getServer().getOnlinePlayers()) {
								p.setLevel(If.getServer().getOnlinePlayers()
										.toArray().length);
							}
						}
						if (If.getServer().getOnlinePlayers().toArray().length >= If
								.getMinPlayers()) {
							initRoundStart();
						}
					}
				}, 20L, 10L);
	}

	/**
	 * Initializes the Round 
	 */
	public void initRoundStart() {
		InitRoundTaskID = If.getServer().getScheduler()
				.scheduleSyncRepeatingTask(If, new Runnable() {

					@Override
					public void run() {
						if (CountDown > 1) {
							CountDown--;
							if (CountDown == 1)
								CountDown--;
							for (Player p : If.getServer().getOnlinePlayers()) {
								p.setLevel(CountDown);
							}
						}
						if (CountDown == 0) {
							If.getServer().broadcastMessage(
									If.getChatPrefix() + ChatColor.BLUE
											+ "Runde startet jetzt !");
							if (Bukkit.getOnlinePlayers().toArray().length >= If
									.getMinPlayers()) {
								Map m = new Map(If, If.getLobby().getMapVote()
										.getWorldWithMostVotes());// TODO write
																	// method
								If.setGame(new Game(If, m));
							} else {
								If.getServer().broadcastMessage(
										If.getChatPrefix()
												+ ChatColor.RED
												+ "Nicht gen�gend Spieler ! "
												+ "Es fehlen noch "
												+ ChatColor.WHITE
												+ (If.getMinPlayers() - If
														.getServer()
														.getOnlinePlayers()
														.toArray().length)
												+ ChatColor.RED + " Spieler 1");

							}
						}
					}

				}, 0L, 20L);
	}

	/**
	 * Initializes the Items for the Player Inventory
	 */
	private void createItems() {
		// ///// Class Selector \\\\\\\\\\\
		this.ClassSelector = new ItemStack(Material.WATCH, 1);
		ItemMeta ClassSelectorMeta = ClassSelector.getItemMeta();
		ClassSelectorMeta.setDisplayName(ChatColor.YELLOW + "Klasse w�hlen");
		List<String> ClassSelectorList = new ArrayList<String>();
		ClassSelectorList.add(ChatColor.GREEN + "W�hle deine Klasse aus");
		ClassSelectorMeta.setLore(ClassSelectorList);
		this.ClassSelector.setItemMeta(ClassSelectorMeta);
		// ///// Map Vote \\\\\\\\\\\\\\
		this.MapVoter = new ItemStack(Material.BOOK, 1);
		ItemMeta MapVoterMeta = MapVoter.getItemMeta();
		MapVoterMeta.setDisplayName(ChatColor.GREEN + "Karten Abstimmung");
		List<String> MapVoterLore = new ArrayList<String>();
		MapVoterLore.add(ChatColor.GREEN + "Stimme f�r eine Karte");
		MapVoterMeta.setLore(MapVoterLore);
		this.MapVoter.setItemMeta(MapVoterMeta);
	}
	
	/**
	 * Resets the Countdown
	 */
	public void resetCountDown(){
		this.CountDown = 120;
	}
	// /////Getter\\\\\\\\\\

	/**
	 * @return Current InfectionWars Instance
	 */
	public InfectionWars getInfectionWars() {
		return this.If;
	}

	/**
	 * @return Lobby World
	 */
	public World getLobbyWorld() {
		return this.lobbyWorld;
	}

	/**
	 * @return Lobby Spawn where every Player gets teleported to on join - set
	 *         in Lobby constructor
	 */
	public Location getSpawnLocation() {
		return this.lobbySpawn;
	}

	/**
	 * @return Lobby Listener Instance
	 */
	public LobbyListener getLobbyListener() {
		return this.lobbylistener;
	}

	public MapVote getMapVote() {
		return this.vote;
	}

	/**
	 * @return the onlinePlayerBoard
	 */
	public PlayerScoreboard getPlayerBoard() {
		return this.OnlinePlayerBoard;
	}
	
	/**
	 * @return Scoreboard Updater Task ID
	 */
	public int getScoreboardUpdaterTaskID(){
		return this.ScoreboardUpdaterTaskID;
	}
	
	/**
	 * @return Class Selector
	 */
	public ClassSelector getClassSelector(){
		return this.selector;
	}
	
	/**
	 * @return InitRoundTaskID
	 */
	public int getInitRoundTaskID(){
		return this.InitRoundTaskID;
	}
	
	/**
	 * @return PlayerScoreboard
	 */
	public PlayerScoreboard getPlayerScoreboard(){
		return this.OnlinePlayerBoard;
	}
	/**
	 * @return Class Selector Item
	 */
	public ItemStack getClassSelectorObject(){
		return ClassSelector;
	}
	public ItemStack getMapVoteObject(){
		return this.MapVoter;
	}
}