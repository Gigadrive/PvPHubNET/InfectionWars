package net.pvp_hub.plugin.Infectionwars.lobby;

import net.pvp_hub.plugin.Infectionwars.main.InfectionWars;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.Event.Result;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.potion.PotionEffect;

/**
 * Listener registered if there is an running Lobby instance
 * 
 * @author Dubtrackz
 *
 */
public class LobbyListener implements Listener {
	private InfectionWars If;

	/**
	 * LobbyListener Constructor
	 * 
	 * @param If
	 *            InfectionWars current Plugin Instance
	 */
	public LobbyListener(InfectionWars If) {
		this.If = If;
		// Registers LobbyListener
		this.If.getServer().getPluginManager().registerEvents(this, If);
		this.If.getLogger().info("Registering LobbyListener .");
	}

	// /////////EventHandler Section\\\\\\\\\\\\\\\\\

	/**
	 * Player Join Event
	 * 
	 * @param e
	 */
	@EventHandler(priority = EventPriority.HIGH)
	public void onPlayerJoin(PlayerJoinEvent e) {
		e.getPlayer().getInventory().setArmorContents(null);
		e.getPlayer().setAllowFlight(false);
		e.getPlayer().setFlying(false);
		e.getPlayer().getInventory().clear();
		e.getPlayer().setExp(0);
		for (PotionEffect effect : e.getPlayer().getActivePotionEffects()) {
			e.getPlayer().removePotionEffect(effect.getType());
		}
		e.getPlayer().sendMessage(
				If.getChatPrefix() + "Wilkommen bei InfectionWars ! ");
		e.setJoinMessage(If.getChatPrefix() + ChatColor.YELLOW
				+ e.getPlayer().getDisplayName() + ChatColor.RESET
				+ " ist der Lobby beigetreten ["
				+ If.getServer().getOnlinePlayers().toArray().length + "/"
				+ If.getMaxPlayers() + "]");
		If.getServer().getScheduler()
				.scheduleSyncDelayedTask(If, new Runnable() {
					@Override
					public void run() {
						e.getPlayer()
								.teleport(If.getLobby().getSpawnLocation());
						e.getPlayer()
								.setGameMode(org.bukkit.GameMode.ADVENTURE);
						e.getPlayer().setFoodLevel(20);
						e.getPlayer().setHealth(20);
						e.getPlayer().setScoreboard(If.getLobby().getPlayerScoreboard().getScoreboard());
						If.getLobby().addLobbyItems(e.getPlayer());
						e.getPlayer().setLevel(0);
					}
				}, 5L);
	}

	/**
	 * Cancel Spawn Event if not Custom or Spawn Egg
	 * 
	 * @param e
	 */
	@EventHandler
	public void onCreatureSpawn(CreatureSpawnEvent e) {
		if (!(e.getSpawnReason().equals(SpawnReason.CUSTOM) && !(e.getSpawnReason().equals(SpawnReason.EGG)))) {
			e.setCancelled(true);
		}
	}

	/**
	 * Prevents Player from Damage except Suicide
	 * 
	 * @param e
	 */
	@EventHandler
	public void preventPlayerDamage(EntityDamageEvent e) {
		if ((!e.getCause().equals(DamageCause.SUICIDE))
				&& (e.getEntity() instanceof org.bukkit.entity.Player)) {
			e.setCancelled(true);
		}
	}

	/**
	 * Sets Quit Message and clears Inventory
	 * @param e
	 */
	@EventHandler
	public void onDisconnect(PlayerQuitEvent e) {//TODO Remove vote from Player
		e.getPlayer().getInventory().clear();
		e.setQuitMessage(If.getChatPrefix() + ChatColor.YELLOW + e.getPlayer().getDisplayName()
				+ ChatColor.RESET + " hat die Lobby verlassen ["
				// -1 because of player isn't unregistered when event is triggered
				+ (If.getServer().getOnlinePlayers().toArray().length - 1)
				+ "/" + If.getMaxPlayers() + "]");
	}
	/**
	 * Players who are not OP have no Permissions to break blocks
	 */
	@EventHandler
	public void buildPermissions(BlockBreakEvent e){
		if(!e.getPlayer().isOp()){
			e.setCancelled(true);
		}
	}
	/**
	 * Non OP Player cant place Blocks
	 */
	@EventHandler
	public void blockPlacePermissions(BlockPlaceEvent e){
		if(!e.getPlayer().isOp()){
			e.setCancelled(true);
		}
	}
	/**
	 * Non Op Players can't drop items
	 */
	@EventHandler
	public void noDrops(PlayerDropItemEvent e){
		if(!e.getPlayer().isOp()){
			e.setCancelled(true);
		}
	}
	/**
	 * Reset Foodlevel from Player
	 */
	@EventHandler
	public void noHunger(FoodLevelChangeEvent e){
		if(e.getEntity() instanceof Player){
			e.setFoodLevel(20);
		}
	}
	@EventHandler
	public void onChoseClass(InventoryClickEvent e){
		Player p;
		if(e.getWhoClicked() instanceof Player){
			p = (Player)e.getWhoClicked();
		}else{
			If.getLogger().info("There is an Error with Entity Handling ! - An unhuman Thing want to choose a Class or vote for a Map :O");
			return;
		}
		if(e.getCurrentItem() == null){
			return;
		}
		if(e.getCurrentItem().equals(If.getLobby().getClassSelector().getAcceptButton())){
			if(If.getLobby().getClassSelector().PlayerHasChosen(p)){
				p.sendMessage(If.getChatPrefix()+ChatColor.RED+"Du hast deine Klasse bereits ausgew�hlt !");
				e.setCancelled(true);
				p.closeInventory();
			}else{
				If.getLobby().getClassSelector().addPlayerClass(p, If.getLobby().getClassSelector().getClassIdByName(e.getInventory().getTitle()));
				p.sendMessage(If.getChatPrefix()+ChatColor.GREEN+"Du hast die Klasse "+ChatColor.WHITE+If.getLobby().getClassSelector().getClassNameByID(If.getLobby().getClassSelector().getClassIdByName(e.getInventory().getTitle()))+ChatColor.GREEN+" gew�hlt !");
				p.closeInventory();
			}	
		}else if(e.getCurrentItem().equals(If.getLobby().getClassSelector().getDeclineButton())){
			p.closeInventory();
		}else{
			e.setResult(Result.DENY);//TODO Teleport entity
			e.setCancelled(true);
			delayedCloseInventory(p);
		}
		if(e.getInventory().equals(If.getLobby().getMapVote().getInventory())){
			if(If.getLobby().getMapVote().isValidMap(e.getCurrentItem())){
				if(If.getLobby().getMapVote().hasUserVoted(p)){
					p.sendMessage(If.getChatPrefix()+ChatColor.RED+"Du hast bereits f�r eine Karte gestimmt !");
					e.setCancelled(true);
					p.closeInventory();
				}else{
					If.getLobby().getMapVote().addUserVote(p, e.getSlot());
					p.sendMessage(If.getChatPrefix()+ChatColor.GREEN+"Du hast f�r die Karte "+ChatColor.WHITE+If.getLobby().getMapVote().getMapNameByID(e.getSlot())+ChatColor.GREEN+" gestimmt !");
					p.closeInventory();
					If.getLobby().getPlayerScoreboard().addUserVote(e.getSlot());
				}
			}
		}
	}
	/**
	 * @param e
	 */
	@EventHandler
	public void openInventories(PlayerInteractEvent e){
		Player p = e.getPlayer();
		if(e.getAction() == Action.RIGHT_CLICK_BLOCK || e.getAction() == Action.LEFT_CLICK_BLOCK){
			Block b = e.getClickedBlock();
			if(p.getItemInHand().equals(If.getLobby().getMapVoteObject())){
				p.openInventory(If.getLobby().getMapVote().getInventory());
			}
			if(b.getType() == Material.WALL_SIGN || b.getType() == Material.SIGN_POST){
				Sign s = (Sign)b.getState();
				String[] lines = s.getLines();
				if(lines[0].equals("[Class]")){
					p.openInventory(If.getLobby().getClassSelector().getClassPreViewInventory(If.getLobby().getClassSelector().getClassIdByName(lines[1])));
				}
			}
		}
	}
	@EventHandler
	public void preventCreeperBlockDamage(EntityExplodeEvent e){
		e.setCancelled(true);
	}
	
	private void delayedCloseInventory(Player p){
		If.getServer().getScheduler().scheduleSyncDelayedTask(If, new Runnable(){

			@Override
			public void run() {
				p.closeInventory();
				
			}
			
		}, 1L);
	}
	// //////////Getter\\\\\\\\\\\\\\\\
	/**
	 * @return Current InfectionWars Plugin Instance
	 */
	public InfectionWars getInfectionWarsInstance() {
		return this.If;
	}

	/**
	 * Unregisters LobbyListerner
	 */
	public void unregisterLobbyListener() {
		this.If.getServer().getLogger().info("Unregistering LobbyListener");
		HandlerList.unregisterAll(this);
	}
}
