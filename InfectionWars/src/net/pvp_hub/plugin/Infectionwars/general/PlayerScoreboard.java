package net.pvp_hub.plugin.Infectionwars.general;

import java.util.HashMap;

import net.pvp_hub.plugin.Infectionwars.main.InfectionWars;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Criterias;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

public class PlayerScoreboard {
	private Scoreboard board;
	private final InfectionWars If;
	private Objective MapVoteObj;
	private HashMap<Player, Integer> PlayerScores;
	private int ScoreBoardUpdaterTaskID;
	private Objective Scores;
	private Objective Health;
	private Objective HealthTabList;
	private final int Type;

	public PlayerScoreboard(InfectionWars p, int Type) {
		this.If = p;
		this.Type = Type;
		init();
		setupScoreboard();

	}
	public void setWaveInNameAndTime(int Wave,String minutes,String seconds){
		Scores.setDisplayName(minutes+":"+seconds+"  [Wave"+Wave+"]");
	}
	@SuppressWarnings("deprecation")
	public void addPlayerScore(Player p, String Entity) {
		String Name = p.getName();
		if (Name.length() > 13) {
			Name = Name.substring(0, 13);
		}
		int aa = PlayerScores.get(p);
		aa = aa + If.getConfigHandler().getScoreToAdd(p, Entity);
		PlayerScores.put(p, aa);
		Score s = Scores.getScore(If.getServer().getOfflinePlayer(ChatColor.GREEN
				+ Name));
		s.setScore(aa);
	}
	@SuppressWarnings("deprecation")
	public void addUserVote(int MapID) {
		Score s;
		switch (MapID) {
		case 0:
			s = MapVoteObj.getScore(If.getServer().getOfflinePlayer(ChatColor.GREEN
					+ "Airport"));
			break;
		case 1:
			s = MapVoteObj.getScore(If.getServer().getOfflinePlayer(ChatColor.GREEN
					+ "Ruins"));
			break;
		case 2:
			s = MapVoteObj.getScore(If.getServer().getOfflinePlayer(ChatColor.GREEN
					+ "Karte 3"));
			break;
		default:
			s = MapVoteObj.getScore(If.getServer().getOfflinePlayer(ChatColor.GREEN
					+ "Unknow"));
		}
		s.setScore(s.getScore() + 1);
	}

	public int getPlayerScore(Player p) {
		return PlayerScores.get(p);
	}

	public HashMap<Player, Integer> getPlayerScores() {
		return PlayerScores;
	}

	public InfectionWars getPlugin() {
		return If;
	}

	public Scoreboard getScoreboard() {
		return board;
	}

	public int getScoreBoardUpdaterID() {
		return ScoreBoardUpdaterTaskID;
	}

	private void init() {
		PlayerScores = new HashMap<Player, Integer>();
	}
	@SuppressWarnings("deprecation")
	public void markPlayerAsAlive(Player p){
		String PlayerName = p.getName();
		if (PlayerName.length() > 13) {
			PlayerName = PlayerName.substring(0, 13);
		}
		board.resetScores(If.getServer().getOfflinePlayer(ChatColor.RED + PlayerName));
		Score s = Scores.getScore(If.getServer().getOfflinePlayer(ChatColor.GREEN
				+ PlayerName));
		s.setScore(getPlayerScore(p));
	}
	
	@SuppressWarnings("deprecation")
	public void markPlayerAsDead(Player p) {
		String PlayerName = p.getName();
		if (PlayerName.length() > 13) {
			PlayerName = PlayerName.substring(0, 13);
		}
		board.resetScores(If.getServer().getOfflinePlayer(ChatColor.GREEN + PlayerName));
		Score s = Scores.getScore(If.getServer().getOfflinePlayer(ChatColor.RED
				+ PlayerName));
		s.setScore(getPlayerScore(p));
	}
	@SuppressWarnings("deprecation")
	private void setupScoreboard() {
		board = If.getServer().getScoreboardManager().getNewScoreboard();
		if (Type == 1) {
			MapVoteObj = board.registerNewObjective("mapvotes", "dummy");
			MapVoteObj.setDisplayName("Karten Abstimmung :");
			MapVoteObj.setDisplaySlot(DisplaySlot.SIDEBAR);
			Score Map1 = MapVoteObj.getScore(If.getServer()
					.getOfflinePlayer(ChatColor.GREEN + "Airport"));
			Score Map2 = MapVoteObj.getScore(If.getServer()
					.getOfflinePlayer(ChatColor.GREEN + "Ruins"));
			Score Map3 = MapVoteObj.getScore(If.getServer()
					.getOfflinePlayer(ChatColor.GREEN + "Karte 3"));
			Map1.setScore(0);
			Map2.setScore(0);
			Map3.setScore(0);
		} else if (Type == 2) {
			HealthTabList = board.registerNewObjective("TabListHealth", Criterias.HEALTH);
			HealthTabList.setDisplaySlot(DisplaySlot.PLAYER_LIST);
			HealthTabList.setDisplayName("HP");
			Health = board.registerNewObjective("showhealth", Criterias.HEALTH);
			Health.setDisplaySlot(DisplaySlot.BELOW_NAME);
			Health.setDisplayName(" / 20 HP");
			Scores = board.registerNewObjective("scores", "dummy");
			Scores.setDisplaySlot(DisplaySlot.SIDEBAR); 
			Scores.setDisplayName("Scores : [Wave 1]");
			for (Player p : If.getServer().getOnlinePlayers()) {
				String PlayerName = p.getName();
				if (PlayerName.length() > 13) {
					PlayerName = PlayerName.substring(0, 13);
				}
				Score ps = Scores.getScore(If.getServer()
						.getOfflinePlayer(ChatColor.GREEN + PlayerName));
				ps.setScore(0);
				PlayerScores.put(p, 0);
			}
		}

		for (Player sonline : If.getServer().getOnlinePlayers()) {
			sonline.setScoreboard(board);
		}
	}
}