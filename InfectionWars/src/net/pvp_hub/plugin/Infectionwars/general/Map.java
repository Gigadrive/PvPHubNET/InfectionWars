package net.pvp_hub.plugin.Infectionwars.general;

import java.util.Random;

import net.pvp_hub.plugin.Infectionwars.main.InfectionWars;
import net.pvp_hub.plugin.Infectionwars.main.Objects.Border;

import org.bukkit.Difficulty;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.World.Environment;

/**
 * Map Class with Map Information like spawnpoints etc.
 * 
 * @author Dubtrackz
 *
 */
public class Map {
	private Border border;
	private InfectionWars If;
	private int MapID;

	private World MapWorld;

	private Location[] Spawn;

	public Map(InfectionWars If, int Map) {
		this.MapID = Map;
		this.If = If;
		initBorders(Map);
		initWorld(Map);
		initArray(Map);
	}

	/**
	 * Get the World ID by Name
	 * 
	 * @param name
	 *            Which ID should be looked up
	 * @return World ID
	 */
	public static int getWorldIDbyName(String name) {
		switch (name) {
		case "Airport":
			return Maps.AIRPORT;
		case "Ruins":
			return Maps.RUINS;
		default:
			return 0;
		}
	}

	/**
	 * Get the World name by ID
	 * 
	 * @param id
	 *            Which Name should be looked up
	 * @return World name
	 */
	public static String getWorldNameByID(int id) {
		switch (id) {
		case Maps.AIRPORT:
			return "Airport";
		case Maps.RUINS:
			return "InfWars_Ruins";
		default:
			return "";
		}
	}

	/**
	 * @return Border of the Map
	 */
	public Border getBorder() {
		return this.border;
	}

	// ///////////Getter\\\\\\\\\\\\\\\\\
	/**
	 * @return Current InfectionWars Instance
	 */
	public InfectionWars getInfectionWars() {
		return this.If;
	}

	/**
	 * @return Returns ID of the Map
	 */
	public int getMapID() {
		return this.MapID;
	}

	/**
	 * Get an random Location 
	 * @param size Size of the Creature
	 * @return Random Location within the Border
	 */
	public Location getRandomLocation(int size) {
		Random r = new Random();
		int x = r.nextInt((border.MaxX - size) - (border.MinX + size) + 1)
				+ (border.MinX + size);
		int z = r.nextInt((border.MaxZ - size) - (border.MinZ + size) + 1)
				+ (border.MinZ + size);
		int y = MapWorld.getHighestBlockYAt(x, z);
		
		while(y >= border.MaxY){
			x = r.nextInt((border.MaxX - size) - (border.MinX + size) + 1)
					+ (border.MinX + size);
			z = r.nextInt((border.MaxZ - size) - (border.MinZ + size) + 1)
					+ (border.MinZ + size);
			y = MapWorld.getHighestBlockYAt(x, z);
		}
		return new Location(MapWorld, x + 0.5f, y, z + 0.5f);
	}

	/**
	 * Get the specified Spawnpoint from Array
	 * @param id Specifies the Spawnpoint
	 * @return Location of specified spawnpoint
	 */
	public Location getSpawnLocation(int id) {
		return Spawn[id];
	}

	/**
	 * @return The World 
	 */
	public World getWorld() {
		return MapWorld;
	}

	/**
	 * Initializes the Spawnpoints in an Array
	 * @param id Specifies the Map
	 */
	private void initArray(int id) {
		if (id == Maps.AIRPORT) {
			Spawn = new Location[13];
			Spawn[0] = new Location(MapWorld, -168 + 0.5f, 7, 27 + 0.5f);
			Spawn[1] = new Location(MapWorld, -166 + 0.5f, 7, 27 + 0.5f);
			Spawn[2] = new Location(MapWorld, -168 + 0.5f, 7, 29 + 0.5f);
			Spawn[3] = new Location(MapWorld, -166 + 0.5f, 7, 29 + 0.5f);
			Spawn[4] = new Location(MapWorld, -168 + 0.5f, 7, 31+ 0.5f);
			Spawn[5] = new Location(MapWorld, -166 + 0.5f, 7, 31 + 0.5f);
			Spawn[6] = new Location(MapWorld, -168 + 0.5f, 7, 33 + 0.5f);
			Spawn[7] = new Location(MapWorld, -166 + 0.5f, 7, 33 + 0.5f);
			Spawn[8] = new Location(MapWorld, -168 + 0.5f, 7, 35 + 0.5f);
			Spawn[9] = new Location(MapWorld, -166 + 0.5f, 7, 35 + 0.5f);
			Spawn[10] = new Location(MapWorld, -168 + 0.5f, 7, 37 + 0.5f);
			Spawn[11] = new Location(MapWorld, -166 + 0.5f, 7, 37 + 0.5f);
		} else if (id == Maps.RUINS) {

		}
	}

	/**
	 * Creates an Border Object which can be used by other classes to check the borders
	 * @param id Specifies Map
	 */
	private void initBorders(int id) {
		if (id == Maps.AIRPORT) {
			border = new Border();
			border.MinX = -189;
			border.MinY = 4;
			border.MinZ = -54;
			border.MaxX = -90;
			border.MaxY = 10;
			border.MaxZ = 67;
			border.MaxEntityY = 25;
		} else if (id == Maps.RUINS) {

		}
	}

	/**
	 * Initialize the World and configure the World Enviroment
	 * @param id Specifies the map
	 */
	private void initWorld(int id) {
		if (If.getServer().getWorld(getWorldNameByID(id)) == null) {
			if (id == Maps.AIRPORT) {
				World w = If.getServer().getWorld("InfWarsWorld");
				w.setPVP(false);
				w.setSpawnFlags(false, false);
				w.setDifficulty(Difficulty.NORMAL);
				w.setAutoSave(false);
				this.MapWorld = w;
				System.out.println("Loading Map with ID " + id);
			} else if (id == Maps.RUINS) {
				WorldCreator wc = new WorldCreator("InfWars_Ruins");
				wc.type(WorldType.FLAT);
				wc.environment(Environment.NORMAL);
				World w = wc.createWorld();
				w.setPVP(false);
				w.setSpawnFlags(false, false);
				w.setDifficulty(Difficulty.NORMAL);
				w.setAutoSave(false);
				this.MapWorld = w;
				System.out.println("Loading Map with ID " + id);
			}
		} else {
			if (id == Maps.AIRPORT) {
				this.MapWorld = If.getServer().getWorld("InfWarsWorld");
			} else if (id == Maps.RUINS) {
				this.MapWorld = If.getServer().getWorld("Airport");
			}

		}
	}
}