package net.pvp_hub.plugin.Infectionwars.general;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.pvp_hub.plugin.Infectionwars.main.InfectionWars;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class MapVote {
	private InfectionWars If;
	private Inventory inv;
	private ItemStack Map1;
	private ItemStack Map2;
	private HashMap<Player, Integer> VoteBox;

	public MapVote(InfectionWars plugin) {
		this.If = plugin;
		initInventories();
		init();
	}

	public void addUserVote(Player p, int MapID) {
		VoteBox.put(p, MapID);
	}

	//////////Getter\\\\\\\\\\\\
	public InfectionWars getInfectionWars(){
		return this.If;
	}

	public Inventory getInventory() {
		return this.inv;
	}

	public ItemStack getMap1Object() {
		return this.Map1;
	}

	public ItemStack getMap2Object() {
		return this.Map2;
	}

	public String getMapNameByID(int id) {
		switch (id) {
		case 0:
			return "Map1";
		case 1:
			return "Map2";
		default:
			return "Error";
		}
	}

	public int getWorldWithMostVotes() {
		// Map.Entry<Player, Integer> maxEntry = null;
		//
		// for (Map.Entry<Player, Integer> entry : VoteBox.entrySet())
		// {
		// if (maxEntry == null ||
		// entry.getValue().compareTo(maxEntry.getValue()) > 0)
		// {
		// maxEntry = entry;
		// }
		// }
		// String MapName = maxEntry.getValue();
		return 0;
	}

	public boolean hasUserVoted(Player p) {
		if (VoteBox.containsKey(p)) {
			return true;
		} else {
			return false;
		}
	}
	private void init() {
		VoteBox = new HashMap<Player, Integer>();
	}
	
	private void initInventories() {
		inv = If.getServer().createInventory(null, 18,
				"Stimme f�r eine Karte ab :");
		ItemStack Map1 = new ItemStack(Material.MAP, 1);
		ItemStack Map2 = new ItemStack(Material.MAP, 1);
		ItemMeta Map1Meta = Map1.getItemMeta();
		ItemMeta Map2Meta = Map2.getItemMeta();
		Map1Meta.setDisplayName(ChatColor.GREEN + "Airport");
		Map2Meta.setDisplayName(ChatColor.GREEN + "Ruins");
		List<String> descriptionMap1 = new ArrayList<String>();
		List<String> descriptionMap2 = new ArrayList<String>();
		descriptionMap1.add(ChatColor.WHITE + "Karten Beschreibung ");
		descriptionMap2.add(ChatColor.WHITE + "Karten Beschreibung ");
		Map1Meta.setLore(descriptionMap1);
		Map2Meta.setLore(descriptionMap2);
		Map1.setItemMeta(Map1Meta);
		Map2.setItemMeta(Map2Meta);
		ItemStack[] it = new ItemStack[18];
		it[0] = Map1;
		it[1] = Map2;
		this.Map1 = Map1;
		this.Map2 = Map2;
		inv.setContents(it);
	}

	public boolean isValidMap(ItemStack i) {
		if (i.equals(Map1) || i.equals(Map2)) {
			return true;
		} else {
			return false;
		}
	}
}