package net.pvp_hub.plugin.Infectionwars.general;

/**
 * Class IDs
 * @author Dubtrackz
 *
 */
public class Class {
	public static int ALLROUND = 4;
	public static int ARCHER = 1;
	public static int CHEMIST = 0;
	public static int FIGHTER = 5;
	public static int HEAVY = 3;
	public static int KNIGHT = 2;
	public static int STATUE = 6;
	public static int SURVIVOR = 7;
	public static int VIPS = 8;
}
