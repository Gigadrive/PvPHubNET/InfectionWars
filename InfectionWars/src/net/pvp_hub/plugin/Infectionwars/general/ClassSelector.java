package net.pvp_hub.plugin.Infectionwars.general;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

import net.pvp_hub.plugin.Infectionwars.main.InfectionWars;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * Class Selector Object 
 * @author Dubtrackz
 *
 */
public class ClassSelector {
/*	private ItemStack AllRound;
	private ItemStack Archer;
	private ItemStack Chemist;
	private ItemStack Fighter;
	private ItemStack Heavy;
	private Inventory inv;
	private ItemStack Knight;*/
	private Inventory chemistInv;
	private Inventory archerInv;
	private Inventory knightInv;
	private Inventory heavyInv;
	private Inventory allrounderInv;
	private Inventory fighterInv;
	private Inventory statueInv;
	private Inventory survivorInv;
	private ItemStack acceptButton;
	private ItemStack declineButton;
	//Stores the Class which the player has chosen
	private HashMap<Player, Integer> PlayerClasses = new HashMap<Player, Integer>();
/*	private ItemStack Statue;
	private ItemStack Survivor;
	private ItemStack Vips;*/
	private InfectionWars If;

	public ClassSelector(InfectionWars If) {
		this.If = If;
		//init();
		initInventories();
	}

	/**
	 * Stores the Class which the Player has chosen
	 * @param p Specifies the player
	 * @param ClassID Specifies the chosen class
	 */
	public void addPlayerClass(Player p, int ClassID) {
		PlayerClasses.put(p, ClassID);
	}

	/**
	 * Gets the Class Name by ID
	 * @param ID Specifies the class by ID
	 * @return Class name as string looked up by ID
	 */
	public String getClassNameByID(int ID) {
		if (ID == Class.CHEMIST)
			return "Chemist";
		if (ID == Class.ARCHER)
			return "Archer";
		if (ID == Class.KNIGHT)
			return "Knight";
		if (ID == Class.HEAVY)
			return "Heavy";
		if (ID == Class.ALLROUND)
			return "Alleskönner";
		if (ID == Class.FIGHTER)
			return "Kämpfer";
		if (ID == Class.STATUE)
			return "Statue";
		if (ID == Class.SURVIVOR)
			return "Überlebender";
		if (ID == Class.VIPS)
			return "Vips-Babo";
		return null;
	}

	public int getClassIdByName(String name){
		if(name.equalsIgnoreCase("Chemist")){
			return Class.CHEMIST;
		}else if(name.equalsIgnoreCase("Archer")){
			return Class.ARCHER;
		}else if(name.equalsIgnoreCase("Knight")){
			return Class.KNIGHT;
		}else if(name.equalsIgnoreCase("Heavy")){
			return Class.HEAVY;
		}else if(name.equalsIgnoreCase("Allrounder")){
			return Class.ALLROUND;
		}else if(name.equalsIgnoreCase("Fighter")){
			return Class.FIGHTER;
		}else if(name.equalsIgnoreCase("Statue")){
			return Class.STATUE;
		}else if(name.equalsIgnoreCase("Survivor")){
			return Class.SURVIVOR;
		}else{
			return 0;
		}
	}
	/**
	 * @return Class Selector Inventory
	 */
/*	public Inventory getInventory() {
		return inv;
	}*/

	public Inventory getClassPreViewInventory(int ID){

		if (ID == Class.CHEMIST)
			return chemistInv;
		if (ID == Class.ARCHER)
			return archerInv;
		if (ID == Class.KNIGHT)
			return knightInv;
		if (ID == Class.HEAVY)
			return heavyInv;
		if (ID == Class.ALLROUND)
			return allrounderInv;
		if (ID == Class.FIGHTER)
			return fighterInv;
		if (ID == Class.STATUE)
			return statueInv;
		if (ID == Class.SURVIVOR)
			return survivorInv;
		return null;
	}
	/**
	 * Looks up the class which the player has chosen
	 * @param p Specifies the Player 
	 * @return Class the player has chosen
	 */
	public int getPlayerClass(Player p) {
		return PlayerClasses.get(p);
	}

	/**
	 * @return Player Classes Storage Object(HashMap)
	 */
	public HashMap<Player, Integer> getPlayerClasses() {
		return PlayerClasses;
	}

	/**
	 * Creates the Inventory which the Player will see when he uses the Class Selector
	 */
/*	private void init() {
		inv = If.getServer().createInventory(null, 18, "Wähle deine Klasse :");
		// //////Chemist\\\\\\\\\\\
		ItemStack Chemist = new ItemStack(Material.GOLD_SWORD, 1);
		ItemMeta ChemistMeta = (ItemMeta) Chemist.getItemMeta();
		ChemistMeta.setDisplayName(ChatColor.GOLD + "Chemist");
		List<String> ChemistDesc = new ArrayList<String>();
		ChemistDesc.add(ChatColor.WHITE + "#Platzhalter");
		ChemistDesc.add(ChatColor.BLUE + "+ Goldrüstung");
		ChemistMeta.setLore(ChemistDesc);
		Chemist.setItemMeta(ChemistMeta);
		this.Chemist = Chemist;
		// /////Archer\\\\\\\\\\\
		ItemStack Archer = new ItemStack(Material.BOW, 1);
		Archer.addEnchantment(Enchantment.ARROW_INFINITE, 1);
		Archer.addEnchantment(Enchantment.ARROW_DAMAGE, 4);
		Archer.addEnchantment(Enchantment.ARROW_FIRE, 1);
		ItemMeta ArcherMeta = (ItemMeta) Archer.getItemMeta();
		ArcherMeta.setDisplayName(ChatColor.GREEN + "Archer");
		List<String> ArcherDesc = new ArrayList<String>();
		ArcherDesc.add(ChatColor.WHITE + "#Platzhalter");
		ArcherDesc.add(ChatColor.BLUE + "+ Kettenrüstung");
		ArcherMeta.setLore(ArcherDesc);
		Archer.setItemMeta(ArcherMeta);
		this.Archer = Archer;
		// ////Knight\\\\\\\
		ItemStack Knight = new ItemStack(Material.DIAMOND_SWORD, 1);
		Knight.addEnchantment(Enchantment.DAMAGE_ALL, 4);
		ItemMeta KnightMeta = (ItemMeta) Knight.getItemMeta();
		KnightMeta.setDisplayName(ChatColor.BLUE + "Knight");
		List<String> KnightDesc = new ArrayList<String>();
		KnightDesc.add(ChatColor.WHITE + "#Platzhalter");
		KnightDesc.add(ChatColor.BLUE + "+ Eisenrüstung");
		KnightMeta.setLore(KnightDesc);
		Knight.setItemMeta(KnightMeta);
		this.Knight = Knight;
		// /////Heavy\\\\\\\\
		ItemStack Heavy = new ItemStack(Material.IRON_SWORD, 1);
		Heavy.addEnchantment(Enchantment.DAMAGE_ALL, 1);
		ItemMeta HeavyMeta = (ItemMeta) Heavy.getItemMeta();
		HeavyMeta.setDisplayName(ChatColor.YELLOW + "Heavy");
		List<String> HeavyDesc = new ArrayList<String>();
		HeavyDesc.add(ChatColor.WHITE + "#Platzhalter");
		HeavyDesc.add(ChatColor.BLUE + "+ Diamantrüstung(Protection 1)");
		HeavyMeta.setLore(HeavyDesc);
		Heavy.setItemMeta(HeavyMeta);
		this.Heavy = Heavy;
		// ///AllRound
		ItemStack AllRound = new ItemStack(Material.BOW, 1);
		AllRound.addEnchantment(Enchantment.ARROW_INFINITE, 1);
		ItemMeta AllRoundMeta = (ItemMeta) AllRound.getItemMeta();
		AllRoundMeta.setDisplayName(ChatColor.AQUA + "Alleskönner");
		List<String> AllRoundDesc = new ArrayList<String>();
		AllRoundDesc.add(ChatColor.WHITE + "#PlatzHalter");
		AllRoundDesc.add(ChatColor.BLUE + "Bogen + Schwert");
		AllRoundMeta.setLore(AllRoundDesc);
		AllRound.setItemMeta(AllRoundMeta);
		this.AllRound = AllRound;
		// Fighter
		ItemStack Fighter = new ItemStack(Material.DIAMOND_SWORD, 1);
		Fighter.addEnchantment(Enchantment.FIRE_ASPECT, 1);
		Fighter.addEnchantment(Enchantment.DAMAGE_ALL, 5);
		ItemMeta FighterMeta = (ItemMeta) Fighter.getItemMeta();
		FighterMeta.setDisplayName(ChatColor.AQUA + "Kämpfer");
		List<String> FighterDesc = new ArrayList<String>();
		FighterDesc.add(ChatColor.WHITE + "#PlatzHalter");
		FighterDesc.add(ChatColor.RED + "Keine Rüstung");
		FighterMeta.setLore(FighterDesc);
		Fighter.setItemMeta(FighterMeta);
		this.Fighter = Fighter;
		// Statue
		ItemStack Statue = new ItemStack(Material.DIAMOND_HELMET, 1);
		Statue.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
		ItemMeta StatueMeta = (ItemMeta) Statue.getItemMeta();
		StatueMeta.setDisplayName(ChatColor.AQUA + "Statue");
		List<String> StatueDesc = new ArrayList<String>();
		StatueDesc.add(ChatColor.WHITE + "#PlatzHalter");
		StatueMeta.setLore(StatueDesc);
		Statue.setItemMeta(StatueMeta);
		this.Statue = Statue;
		// Survivor
		ItemStack Survivor = new ItemStack(Material.LEATHER_HELMET, 1);
		Survivor.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
		ItemMeta SurvivorMeta = (ItemMeta) Statue.getItemMeta();
		SurvivorMeta.setDisplayName(ChatColor.AQUA + "Überlebender");
		List<String> SurvivorDesc = new ArrayList<String>();
		SurvivorDesc.add(ChatColor.WHITE + "#PlatzHalter");
		SurvivorMeta.setLore(SurvivorDesc);
		Survivor.setItemMeta(SurvivorMeta);
		this.Survivor = Survivor;
		// Vips
		ItemStack Vips = new ItemStack(Material.POTION, 1, (short) 8236);
		ItemMeta VipsMeta = (ItemMeta) Vips.getItemMeta();
		VipsMeta.setDisplayName(ChatColor.AQUA + "Vipsus Babos");
		List<String> VipsDesc = new ArrayList<String>();
		VipsDesc.add(ChatColor.WHITE + "#PlatzHalter");
		VipsMeta.setLore(VipsDesc);
		Vips.setItemMeta(VipsMeta);
		this.Vips = Vips;
		// ///TO Inventory
		ItemStack[] it = new ItemStack[18];
		it[0] = this.Chemist;
		it[1] = this.Archer;
		it[2] = this.Knight;
		it[3] = this.Heavy;
		it[4] = this.AllRound;
		it[5] = this.Fighter;
		it[6] = this.Statue;
		it[7] = this.Survivor;
		// it[8] = this.Vips;
		inv.setContents(it);
	}*/
	
	private void initInventories(){
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(If.getDataFolder()+"/GameClasses/Chemist.InfClass"));
			chemistInv = InventoryStringDeSerializer.StringToInventory(br.readLine());
			br.close();
			br = new BufferedReader(new FileReader(If.getDataFolder()+"/GameClasses/Archer.InfClass"));
			archerInv = InventoryStringDeSerializer.StringToInventory(br.readLine());
			br.close();
			br = new BufferedReader(new FileReader(If.getDataFolder()+"/GameClasses/Knight.InfClass"));
			knightInv = InventoryStringDeSerializer.StringToInventory(br.readLine());
			br.close();
			br = new BufferedReader(new FileReader(If.getDataFolder()+"/GameClasses/Heavy.InfClass"));
			heavyInv = InventoryStringDeSerializer.StringToInventory(br.readLine());
			br.close();
			br = new BufferedReader(new FileReader(If.getDataFolder()+"/GameClasses/Allrounder.InfClass"));
			allrounderInv = InventoryStringDeSerializer.StringToInventory(br.readLine());
			br.close();
			br = new BufferedReader(new FileReader(If.getDataFolder()+"/GameClasses/Chemist.InfClass"));
			chemistInv = InventoryStringDeSerializer.StringToInventory(br.readLine());
			br.close();
			br = new BufferedReader(new FileReader(If.getDataFolder()+"/GameClasses/Fighter.InfClass"));
			fighterInv = InventoryStringDeSerializer.StringToInventory(br.readLine());
			br.close();
			br = new BufferedReader(new FileReader(If.getDataFolder()+"/GameClasses/Statue.InfClass"));
			statueInv = InventoryStringDeSerializer.StringToInventory(br.readLine());
			br.close();
			br = new BufferedReader(new FileReader(If.getDataFolder()+"/GameClasses/Survivor.InfClass"));
			survivorInv = InventoryStringDeSerializer.StringToInventory(br.readLine());
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		/////Add buttons to Inventory
		acceptButton = new ItemStack(Material.EMERALD_BLOCK,1);
		ItemMeta im = (ItemMeta)acceptButton.getItemMeta();
		im.setDisplayName(ChatColor.GREEN+"Verwenden");
		acceptButton.setItemMeta(im);
		declineButton = new ItemStack(Material.REDSTONE_BLOCK,1);
		im = (ItemMeta)declineButton.getItemMeta();
		im.setDisplayName(ChatColor.RED+"Zur Auswahl");
		declineButton.setItemMeta(im);
		Inventory iv;
		ItemStack[] contents;
		iv = If.getServer().createInventory(null, 54,"Chemist");
		contents = chemistInv.getContents();
		contents[25] = acceptButton;
		contents[34] = declineButton;
		iv.setContents(contents);
		chemistInv = iv;
		iv = If.getServer().createInventory(null, 54,"Archer");
		contents = archerInv.getContents();
		contents[25] = acceptButton;
		contents[34] = declineButton;
		iv.setContents(contents);
		archerInv = iv;
		iv = If.getServer().createInventory(null, 54,"Knight");
		contents = knightInv.getContents();
		contents[25] = acceptButton;
		contents[34] = declineButton;
		iv.setContents(contents);
		knightInv = iv;
		iv = If.getServer().createInventory(null, 54,"Heavy");
		contents = heavyInv.getContents();
		contents[25] = acceptButton;
		contents[34] = declineButton;
		iv.setContents(contents);
		heavyInv = iv;
		iv = If.getServer().createInventory(null, 54,"Allrounder");
		contents = allrounderInv.getContents();
		contents[25] = acceptButton;
		contents[34] = declineButton;
		iv.setContents(contents);
		allrounderInv = iv;
		iv = If.getServer().createInventory(null, 54,"Fighter");
		contents = fighterInv.getContents();
		contents[25] = acceptButton;
		contents[34] = declineButton;
		iv.setContents(contents);
		fighterInv = iv;
		iv = If.getServer().createInventory(null, 54,"Statue");
		contents = statueInv.getContents();
		contents[25] = acceptButton;
		contents[34] = declineButton;
		iv.setContents(contents);
		statueInv = iv;
		iv = If.getServer().createInventory(null, 54,"Survivor");
		contents = survivorInv.getContents();
		contents[25] = acceptButton;
		contents[34] = declineButton;
		iv.setContents(contents);
		survivorInv = iv;
	}
	
	public ItemStack getAcceptButton(){
		return acceptButton;
	}
	
	public ItemStack getDeclineButton(){
		return declineButton;
	}
	
	/**
	 * Checks if the chosen class is valid
	 * @param i specifies the clicked class
	 * @return is the class vaild or not
	 */
/*	public boolean isValidClass(ItemStack i) {
		if (i.equals(Chemist) || i.equals(Archer) || i.equals(Knight)
				|| i.equals(Heavy) || i.equals(AllRound) || i.equals(Statue)
				|| i.equals(Survivor) || i.equals(Fighter) || i.equals(Vips)) {
			return true;
		} else {
			return false;
		}
	}*/

	/**
	 * Checks if a Player has already chosen a class
	 * @param p Specifies the Player
	 * @return Has Player already chosen a class or not 
	 */
	public boolean PlayerHasChosen(Player p) {
		return (PlayerClasses.containsKey(p));
	}
}
