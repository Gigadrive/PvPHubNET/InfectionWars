package net.pvp_hub.plugin.Infectionwars.main;

import net.pvp_hub.plugin.Infectionwars.game.Game;
import net.pvp_hub.plugin.Infectionwars.lobby.Lobby;

import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;
/**
 * InfectionWars is an exclusive MobArena plguin for Pvp-hub.net written by Dubtrackz based 
 * on ideas from VipsNiceGameplay
 * Source was completly rewrote
 * @author Main
 * @version 2.0.0pre1
 */
public class InfectionWars extends JavaPlugin{
	private int maxPlayerAmount;
	private int minPlayerAmount;
	private String ChatPrefix;
	private ConfigHandler confs;
	private Lobby currentLobbyInstance;
	private Game currentGameInstance;
	/* (non-Javadoc)
	 * @see org.bukkit.plugin.java.JavaPlugin#onEnable()
	 */
	@Override
	public void onEnable(){
		this.getLogger().info("Starting InfectionWars Server !");
		MainCommandExecutor cmdexe = new MainCommandExecutor(this);
		this.getCommand("startgame").setExecutor(cmdexe);
		this.getCommand("GiveItems").setExecutor(cmdexe);
		this.getCommand("SetMonsterTaget").setExecutor(cmdexe);
		this.getCommand("setWave").setExecutor(cmdexe);
		this.getCommand("revive").setExecutor(cmdexe);
		confs = new ConfigHandler(this);
		currentLobbyInstance = new Lobby(this,this.getServer().getWorld("InfWarsWorld"));
		currentGameInstance = null;
		ChatPrefix = ChatColor.DARK_GREEN+"[InfectionWars] ";
		maxPlayerAmount = 12;
		minPlayerAmount = 6;
		getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
	}
	
	/* (non-Javadoc)
	 * @see org.bukkit.plugin.java.JavaPlugin#onDisable()
	 */
	@Override 
	public void onDisable(){
		// Free resources 
		System.gc();
	}
	/////////Getter\\\\\\\\\\\\\
	/**
	 * @return Chat Prefix for Messages
	 */
	public String getChatPrefix(){
		return this.ChatPrefix;
	}
	
	/**
	 * @return Current Lobby Instance - Null if in-game
	 */
	public Lobby getLobby(){
		return this.currentLobbyInstance;
	}
	
	/**
	 * @return Current Game Instance - Null if no game running
	 */
	public Game getGame(){
		return this.currentGameInstance;
	}
	/////////Setter\\\\\\\\\\\\\
	
	/**
	 * @param l Lobby Instance which will be set as Current
	 */
	public void setLobby(Lobby l){
		this.currentLobbyInstance = l;
	}
	
	/**
	 * @param g Game Instance which will be set as Current / Main Game Instance
	 */
	public void setGame(Game g){
		this.currentGameInstance = g;
	}
	
	/**
	 * @return InfectionWars ConfigHandler Instance
	 */
	public ConfigHandler getConfigHandler(){
		return this.confs;
	}
	
	/**
	 * @return Max Allowed Players
	 */
	public int getMaxPlayers(){
		return this.maxPlayerAmount;
	}
	
	/**
	 * @return Min needed Players
	 */
	public int getMinPlayers(){
		return this.minPlayerAmount;
	}
}
