package net.pvp_hub.plugin.Infectionwars.main;

/**
 * @author Dubtrackz
 *
 */
public class Objects {
    /**
     * Border Object includes the Worlds Border
     * @author Dubtrackz
     */
    static public class Border {
    	public int MinX;
        public int MinY;
        public int MinZ;
        public int MaxX;
        public int MaxY;
        public int MaxZ;
        public int MaxEntityY;
    }
   /**
 * Storage object for Player Scores
 * @author Dubtrackz
 */
    static public class PointsObject {
    	public PointsObject(int i){
    		if(i == 0){
    			zombie = 0;
    			creeper = 0;
    			spider = 0;
    			skeleton = 0;
    			bat = 0;
    			loadedCreeper = 0;
    			blaze = 0;
    			ghast = 0;
    			witch = 0;
    			wither = 0;
    			cavespider = 0;
    		}
    	}
    	public int zombie;
    	public int creeper;
    	public int spider;
    	public int skeleton;
    	public int bat;
    	public int loadedCreeper;
    	public int blaze;
    	public int ghast;
    	public int wither;
    	public int cavespider;
    	public int witch;
    }
}