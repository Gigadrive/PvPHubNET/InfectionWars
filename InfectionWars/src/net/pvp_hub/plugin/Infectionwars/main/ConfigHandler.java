package net.pvp_hub.plugin.Infectionwars.main;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import net.pvp_hub.plugin.Infectionwars.main.Objects.PointsObject;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Handler for .Yml Config Files
 * 
 * @author Dubtrackz
 */

public class ConfigHandler {
	private FileConfiguration ChestItems;
	private File ChestItemsFile;
	/**
	 * {@value #PointsList} PointsLists stores Points for every Player with use
	 * of {@see net.pvp_hub.plugin.Infectionwars.Objects.PointsObject}
	 */
	private final InfectionWars If;
	private FileConfiguration Points;
	private File PointsFile;
	private HashMap<Player, PointsObject> PointsList;
	private File WaveFile;
	private FileConfiguration WaveList;

	/**
	 * Constructor for ConfigHandler
	 * 
	 * @param If
	 *            InfectionWars current plugin Instance
	 */
	public ConfigHandler(InfectionWars If) {
		this.If = If;
		reloadWaveList();
		FirstRun();
	}

	/**
	 * Addes one kill for the specified entity
	 * 
	 * @param p
	 *            Player of which one kill should be added
	 * @param entity
	 *            Specified Entity which get +1 at player score list (Storage)
	 */
	public void addEntityKill(Player p, String entity) {
		entity = entity.toLowerCase();
		// Gets PointsObject from specified player in Storage
		PointsObject po = PointsList.get(p);
		switch (entity) {
		case "zombie":
			po.zombie += 1;
			break;
		case "creeper":
			po.creeper += 1;
			break;
		case "spider":
			po.spider += 1;
			break;
		case "skeleton":
			po.skeleton += 1;
			break;
		case "bat":
			po.bat += 1;
			break;
		case "loadedcreeper":
			po.loadedCreeper += 1;
			break;
		case "blaze":
			po.blaze += 1;
			break;
		case "ghast":
			po.ghast += 1;
			break;
		case "wither":
			po.wither += 1;
			break;
		case "cavespider":
			po.cavespider += 1;
			break;
		case "witch":
			po.witch += 1;
			break;
		default:
			break;
		}
	}

	/**
	 * Creates Example Configuration Files if it is the Plugins first run and
	 * saves it
	 */
	private void FirstRun() {
		if (WaveFile.length() == 0) {
			WaveList.set("general.waveamount", 1);
			WaveList.set("wave1.zombie", 5);
			WaveList.set("wave1.creeper", 3);
			WaveList.set("wave1.spider", 0);
			WaveList.set("wave1.skeleton", 0);
			WaveList.set("wave1.bat", 0);
			WaveList.set("wave1.loadedcreeper", 0);
			WaveList.set("wave1.blaze", 0);
			WaveList.set("wave1.ghast", 0);
			WaveList.set("wave1.wither", 0);
			WaveList.set("wave1.cavespider", 0);
			WaveList.set("wave1.witch", 0);
		}
		if (PointsFile.length() == 0) {
			Points.set("points.zombie", 0);
			Points.set("points.creeper", 0);
			Points.set("points.spider", 0);
			Points.set("points.skeleton", 0);
			Points.set("points.bat", 0);
			Points.set("points.loadedcreeper", 0);
			Points.set("points.blaze", 0);
			Points.set("points.ghast", 0);
			Points.set("points.wither", 0);
			Points.set("points.cavespider", 0);
			Points.set("points.witch", 0);
		}
		if (ChestItemsFile.length() == 0) {
			ChestItems.set("wave1.chestamount", 3);
			ChestItems.set("wave1.bread", 3);
			ChestItems.set("wave1.stonesword", 1);
		}

		saveFiles();
	}

	/**
	 * Reads the amount of bread which will be in the chests after the specified
	 * wave
	 * 
	 * @param wave
	 *            Specifies the wave
	 * @return The amount of Bread which will be in every chest
	 */
	public int getBread(int wave) {
		return ChestItems.getInt("wave" + wave + ".bread");
	}

	/**
	 * Reads the amount of chests which will spawn after the specified Wave
	 * 
	 * @param wave
	 *            Specifies the Wave
	 * @return
	 */
	public int getChestAmount(int wave) {
		return ChestItems.getInt("wave" + wave + ".chestamount");
	}

	/**
	 * @deprecated
	 * @param Wave
	 * @return
	 */
	public ItemStack[] getChestContents(int Wave) {
		ItemStack[] i = new ItemStack[10];
		return i;
	}

	/**
	 * Loads ChestItemFile if unloaded and returns it
	 * 
	 * @return ChestItemFile FileConfiguration
	 */
	public FileConfiguration getChestItems() {
		if (ChestItems == null) {
			reloadWaveList();
		}
		return ChestItems;
	}

	/**
	 * 
	 * 
	 * @param p
	 *            Specifies which Player score should be looked up
	 * @param entity
	 *            Specifies the Entity
	 * @return Amount of how many specified entities the player had already
	 *         killed
	 */
	public int getEntityKillsFromPlayerSeperated(Player p, String entity) {
		entity = entity.toLowerCase();
		// Gets PointsObject from specified player in Storage
		PointsObject po = PointsList.get(p);
		switch (entity) {
		case "zombie":
			return po.zombie;
		case "creeper":
			return po.creeper;
		case "spider":
			return po.spider;
		case "skeleton":
			return po.skeleton;
		case "bat":
			return po.bat;
		case "loadedcreeper":
			return po.loadedCreeper;
		case "blaze":
			return po.blaze;
		case "ghast":
			return po.ghast;
		case "wither":
			return po.wither;
		case "cavespider":
			return po.cavespider;
		case "witch":
			return po.witch;
		default:
			return 0;
		}
	}

	/**
	 * Reads gernal Waveamount from config File
	 * 
	 * @return Amount of Waves
	 */
	public int getMaxWave() {
		return WaveList.getInt("general.waveamount");
	}

	/**
	 * Get needed entity specific kill amount for X points
	 * 
	 * @param Entity
	 *            Specifies the Entity
	 * @return Needed Amount of Kill for Entity
	 */
	public int getNeededKills(String Entity) {
		Entity = Entity.toLowerCase();
		switch (Entity) {
		case "zombie":
			return Points.getInt("needed.zombie");
		case "creeper":
			return Points.getInt("needed.creeper");
		case "spider":
			return Points.getInt("needed.spider");
		case "skeleton":
			return Points.getInt("needed.skeleton");
		case "bat":
			return Points.getInt("needed.bat");
		case "loadedcreeper":
			return Points.getInt("needed.loadedcreeper");
		case "blaze":
			return Points.getInt("needed.blaze");
		case "ghast":
			return Points.getInt("needed.ghast");
		case "wither":
			return Points.getInt("needed.wither");
		case "cavespider":
			return Points.getInt("needed.cavespider");
		case "witch":
			return Points.getInt("needed.witch");
		default:
			return 0;
		}
	}

	/**
	 * Loads Pointslist if unloaded and returns it
	 * 
	 * @return PointsList FileConfiguration
	 */
	public FileConfiguration getPointsList() {
		if (Points == null) {
			reloadWaveList();
		}
		return Points;
	}

	/**
	 * Method looks up Point Rewards for the specified Entity
	 * 
	 * @param entity
	 *            Specifies Entity
	 * @return Point reward for specified Entity
	 */
	private int getRewardPoints(String entity) {
		entity = entity.toLowerCase();
		switch (entity) {
		case "zombie":
			return Points.getInt("points.zombie");
		case "creeper":
			return Points.getInt("points.creeper");
		case "spider":
			return Points.getInt("points.spider");
		case "skeleton":
			return Points.getInt("points.skeleton");
		case "bat":
			return Points.getInt("points.bat");
		case "loadedcreeper":
			return Points.getInt("points.loadedcreeper");
		case "blaze":
			return Points.getInt("points.blaze");
		case "ghast":
			return Points.getInt("points.ghast");
		case "wither":
			return Points.getInt("points.wither");
		case "cavespider":
			return Points.getInt("points.cavespider");
		case "witch":
			return Points.getInt("points.witch");
		case "win":
			return this.Points.getInt("points.win");
		default:
			return 0;
		}
	}

	/**
	 * Adds Entity kill for check if neeeded kills for a special point amount is
	 * reached<br>
	 * Checks if point amount is reached
	 *
	 * @param p
	 *            Get Player used for Method Operations
	 * @param Entity
	 *            Get Entity Name as String - Used to get needed Kill for amount
	 *            of points from .Yml Config
	 * @return Point amount to add to player score
	 */
	public int getScoreToAdd(Player p, String Entity) {
		addEntityKill(p, Entity);
		// Loads Kills from Yml File - Get how many Entitys from Type "Entity"
		// were already killed by Player
		if (getNeededKills(Entity) == getEntityKillsFromPlayerSeperated(p,
				Entity)) {
			resetEntityKills(p, Entity);
			return getRewardPoints(Entity);
		} else {
			return 0;
		}
	}

	/**
	 * Reads the amount of Creatures which will spawn in the requested wave
	 * 
	 * @param wave
	 *            Wave for which Creatures amount will be looked up
	 * @param entity
	 *            Specifies the Entity which will be looked up
	 * @return
	 */
	public int getWaveAmount(int wave, String entity) {
		entity = entity.toLowerCase();
		switch (entity) {
		case "zombie":
			return WaveList.getInt("wave" + wave + ".zombie");
		case "creeper":
			return WaveList.getInt("wave" + wave + ".creeper");
		case "spider":
			return WaveList.getInt("wave" + wave + ".spider");
		case "skeleton":
			return WaveList.getInt("wave" + wave + ".skeleton");
		case "bat":
			return WaveList.getInt("wave" + wave + ".bat");
		case "loadedcreeper":
			return WaveList.getInt("wave" + wave + ".loadedcreeper");
		case "blaze":
			return WaveList.getInt("wave" + wave + ".blaze");
		case "ghast":
			return WaveList.getInt("wave" + wave + ".ghast");
		case "wither":
			return WaveList.getInt("wave" + wave + ".wither");
		case "cavespider":
			return WaveList.getInt("wave" + wave + ".cavespider");
		case "witch":
			return WaveList.getInt("wave" + wave + ".witch");
		default:
			return 0;
		}
	}

	/**
	 * Reads the amounts of monsters
	 * 
	 * @param wave
	 *            Wave which should looked up
	 * @return The global amount of monsters which will be spawned in the
	 *         specified wave
	 */
	public int getWaveEntityCount(int wave) {
		int i = getWaveAmount(wave, "zombie") + getWaveAmount(wave, "creeper")
				+ getWaveAmount(wave, "spider")
				+ getWaveAmount(wave, "skeleton") + getWaveAmount(wave, "bat")
				+ getWaveAmount(wave, "blaze") + getWaveAmount(wave, "ghast")
				+ getWaveAmount(wave, "wither")
				+ getWaveAmount(wave, "cavespider")
				+ getWaveAmount(wave, "witch");

		return i;
	}

	/**
	 * Loads WaveList if unloaded and returns it
	 * 
	 * @return WaveList FileConfiguration
	 */
	public FileConfiguration getWaveList() {
		if (WaveList == null) {
			reloadWaveList();
		}
		return WaveList;
	}

	/**
	 * Initializes the Player point Storage and puts an empty PointsObject to
	 * Storage for every player
	 * 
	 */
	public void initScores() {
		PointsList = new HashMap<Player, PointsObject>();
		for (Player p : If.getServer().getOnlinePlayers()) {
			PointsObject po = new PointsObject(0);
			PointsList.put(p, po);
		}
	}

	/**
	 * Initialize the Files and (re)loads the .Yml Configuration Files from
	 * /plugins/InfectionWars
	 */
	private void reloadWaveList() {
		if (WaveFile == null) {
			WaveFile = new File(If.getDataFolder(), "Waves.yml");
		}
		if (PointsFile == null) {
			PointsFile = new File(If.getDataFolder(), "Points.yml");
		}
		if (ChestItemsFile == null) {
			ChestItemsFile = new File(If.getDataFolder(), "ChestItems.yml");
		}

		WaveList = YamlConfiguration.loadConfiguration(WaveFile);
		Points = YamlConfiguration.loadConfiguration(PointsFile);
		ChestItems = YamlConfiguration.loadConfiguration(ChestItemsFile);
	}

	/**
	 * Resets Player Kills for a specific Entity
	 * 
	 * @param p
	 *            Player of which kills of entity will be set to 0
	 * @param entity
	 *            String specifies which kills should be set to 0
	 */
	public void resetEntityKills(Player p, String entity) {
		entity = entity.toLowerCase();
		// Gets PointsObject from specified player in Storage
		PointsObject po = PointsList.get(p);
		switch (entity) {
		case "zombie":
			po.zombie = 0;
			break;
		case "creeper":
			po.creeper = 0;
			break;
		case "spider":
			po.spider = 0;
			break;
		case "skeleton":
			po.skeleton = 0;
			break;
		case "bat":
			po.bat = 0;
			break;
		case "loadedcreeper":
			po.loadedCreeper = 1;
			break;
		case "blaze":
			po.blaze = 0;
			break;
		case "ghast":
			po.ghast = 0;
			break;
		case "wither":
			po.wither = 0;
			break;
		case "cavespider":
			po.cavespider = 0;
			break;
		case "witch":
			po.witch = 0;
			break;
		default:
			break;
		}
	}

	/**
	 * Saves Configuration Files and prints Error if one or more lists are
	 * unloaded
	 * 
	 * @throws IOException
	 */
	public void saveFiles() {
		if (WaveFile == null || WaveList == null || PointsFile == null
				|| Points == null || ChestItemsFile == null
				|| ChestItems == null) {
			If.getLogger().info("An Error occurupted while saving WaveList !");
		}

		try {
			WaveList.save(WaveFile);
			Points.save(PointsFile);
			ChestItems.save(ChestItemsFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
