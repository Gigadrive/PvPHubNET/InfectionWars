package net.pvp_hub.plugin.Infectionwars.main;

import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.plugin.Infectionwars.game.Game;
import net.pvp_hub.plugin.Infectionwars.general.Map;
import net.pvp_hub.plugin.Infectionwars.general.Maps;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * Main Command Executor 
 * 
 * @author Dubtrackz
 *
 */
public class MainCommandExecutor implements CommandExecutor{

	private InfectionWars If;
	
	/**
	 * @param If Get InfectionWars Instance of main-class
	 */
	public MainCommandExecutor(InfectionWars If){
		this.If = If;
	}
	
	/* (non-Javadoc)
	 * @see org.bukkit.command.CommandExecutor#onCommand(org.bukkit.command.CommandSender, org.bukkit.command.Command, java.lang.String, java.lang.String[])
	 */
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(command.getName().equalsIgnoreCase("Startgame")){
			If.setGame(new Game(If,new Map(If,Maps.AIRPORT)));
			return true;
		}else if(command.getName().equalsIgnoreCase("GiveItems")){
			If.getLobby().addLobbyItems(If.getServer().getPlayer("crafter14360"));
			return true;
		}else if(command.getName().equalsIgnoreCase("SetMonsterTaget")){
			if(sender.isOp()){
				If.getGame().targetAllCreaturesToOnePlayer(If.getServer().getPlayer(args[0]));
			}
			return true;
		}else if(command.getName().equalsIgnoreCase("setWave")){
			int wave = Integer.parseInt(args[0]);
			If.getGame().setSetWave(wave);
			return true;
		}else if(command.getName().equalsIgnoreCase("revive")){
			if(!(args.length == 2)){
				return false;
			}
			
			if(If.getGame() == null){
				sender.sendMessage(If.getChatPrefix()+ChatColor.DARK_RED+"Das Spiel hat noch nicht begonnen !");
				return true;
			}
			
			Player p = If.getServer().getPlayer(args[0]);
			
			if(!(p.isOnline())){
				sender.sendMessage(If.getChatPrefix()+ChatColor.DARK_RED+"Der Spieler "+ChatColor.WHITE+args[0]+ChatColor.DARK_RED+" ist nicht online !");
				return true;
			}else if(!If.getGame().isPlayerDead(p)){
				sender.sendMessage(If.getChatPrefix()+ChatColor.GREEN+" Der Spieler "+ChatColor.WHITE+args[0]+ChatColor.GREEN+" lebt noch !");
				return true;
			}else{
				if(args[1].equalsIgnoreCase("chips")){
						int chips = 0;
						try {
							chips = PlayerUtilities.getChips(p);
						} catch (Exception e) {
							e.printStackTrace();
						}
						if((chips - If.getGame().getReviveCostsChips()) >=0){
							for(Player pp : If.getGame().getRevivedPlayers()){
								if(pp.equals(p)){
									sender.sendMessage(If.getChatPrefix()+ChatColor.DARK_RED+"Der Spieler wurde bereits ein mal wiederbelebt !");
									return true;
								}
							}
							If.getGame().reviveplayer(p,args[0]);
							try {
								PlayerUtilities.removeChips(p, If.getGame().getReviveCostsChips());
							} catch (Exception e) {
								e.printStackTrace();
							}
							return true;
						}else{
							sender.sendMessage(If.getChatPrefix()+ChatColor.DARK_AQUA+"Du hast nicht gen�gend Chips !");
						}

				}else if(args[1].equalsIgnoreCase("coins")){
						
					int coins = 0;
					try {
						coins = PlayerUtilities.getCoins(p);
					} catch (Exception e) {
						e.printStackTrace();
					}
					if((coins - If.getGame().getReviveCostsCoins()) >=0){
						for(Player pp : If.getGame().getRevivedPlayers()){
							if(pp.equals(p)){
								sender.sendMessage(If.getChatPrefix()+ChatColor.DARK_RED+"Der Spieler wurde bereits ein mal wiederbelebt !");
								return true;
							}
						}
						If.getGame().reviveplayer(p,args[0]);
						try {
							PlayerUtilities.removeCoins(p, If.getGame().getReviveCostsCoins());
						} catch (Exception e) {
							e.printStackTrace();
						}
						return true;
						}else{
							sender.sendMessage(If.getChatPrefix()+ChatColor.DARK_AQUA+"Du hast nicht gen�gend Coins !");
						}
						
				}else{
					sender.sendMessage(If.getChatPrefix()+ChatColor.DARK_RED+"Die W�hrung"+ChatColor.WHITE+args[2] +ChatColor.DARK_RED+"ist ung�ltig ! M�glich sind :[Chips/Coins]");
				}
			}
			return true;
		}
		return false;
	}

	////////////Getter\\\\\\\\\\\\\
	public InfectionWars getInfectionWarsInstance(){
		return this.If;
	}
}
