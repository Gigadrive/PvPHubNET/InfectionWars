 package net.pvp_hub.plugin.Infectionwars.game;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Difficulty;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.entity.Blaze;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Enderman;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Ghast;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Wither;
import org.bukkit.inventory.ItemStack;

import net.pvp_hub.core.PvPHubCore;
import net.pvp_hub.core.api.GameState;
import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.plugin.Infectionwars.general.Map;
import net.pvp_hub.plugin.Infectionwars.general.PlayerScoreboard;
import net.pvp_hub.plugin.Infectionwars.main.ConfigHandler;
import net.pvp_hub.plugin.Infectionwars.main.InfectionWars;

/**
 * @author Dubtrackz
 *
 */
public class Game {
	private int countdownTaskID;
	private int mainTaskID;
	private int timeChangeTaskID;
	private int weatherTaskID;
	private int teleportTaskID;
	private InfectionWars If;
	private GameListener gamelistener;
	private Map map;
	private PlayerScoreboard scoreboard;
	private int livingPlayers;
	private int playerCount;
	private Class_Sets classSets;
	private int Wave;
	private int startCountDown;
	private boolean waitForStartGame;
	private int waveCountDown;
	private boolean ranTask;
	private int setWave;
	private int randomWeather;
	private int weatherTaskRuntimes;
	private int livingMonsters;
	private int reviveCostsChips;
	private int reviveCostsCoins;
	private int minutes;
	private int seconds;
	private ArrayList<Player> revivedPlayers;
	private HashSet<Location> RandomChestPositions;
	public Game(InfectionWars If, Map map) {
		System.gc();
		PvPHubCore.setGameState(GameState.INGAME);
		If.getLobby().getLobbyListener().unregisterLobbyListener();
		this.If = If;
		gamelistener = new GameListener(If);
		this.livingPlayers = 0;
		this.classSets = new Class_Sets(If);
		initializer(map);
		startCountDown = 8;
		setWaitForStartGame(true);
		playerCount = 0;
		Wave = 1;
		waveCountDown = 15;
		ranTask = false;
		randomWeather = 0;
		weatherTaskRuntimes = 0;
		revivedPlayers = new ArrayList<Player>();
		minutes = -1;
		seconds = -1;
		setReviveCostsChips(1);
		setReviveCostsCoins(150);
		setSetWave(-1);
		setLivingMonsters(0);
		removeEntities();
		createTasks();
	}

	private void initializer(Map map) {
		// Kill Lobby Tasks
		If.getLogger().info("Initializing Game");
		If.getServer().getScheduler()
				.cancelTask(If.getLobby().getInitRoundTaskID());
		If.getServer().getScheduler()
				.cancelTask(If.getLobby().getScoreboardUpdaterTaskID());
		this.map = map;
		// Creates new score storage
		If.getConfigHandler().initScores();
		if (map == null)
			If.getLogger().warning("There is an Error with the Map");
		// Map Configs
		for (Entity e : map.getWorld().getEntities()) {
			if (!(e instanceof Player)) {
				e.remove();
			}
		}
		map.getWorld().setAutoSave(false);
		map.getWorld().setPVP(false);
		map.getWorld().setSpawnFlags(false, false);//Change to false, false
		map.getWorld().setDifficulty(Difficulty.NORMAL);
		map.getWorld().setTime(40000);
		map.getWorld().setStorm(false);
		map.getWorld().setThundering(false);
		map.getWorld().setGameRuleValue("doFireTick", "false");

		// Init Scoreboard Type 2
		this.scoreboard = new PlayerScoreboard(If, 2);
		for (Player p : If.getServer().getOnlinePlayers()) {
			if (!If.getLobby().getClassSelector().PlayerHasChosen(p)) {
				If.getLobby().getClassSelector().addPlayerClass(p, 0);
				p.sendMessage(If.getChatPrefix() + ChatColor.RED
						+ "Du hast keine Klasse gew�hlt - Du spielst nun als "
						+ ChatColor.WHITE
						+ If.getLobby().getClassSelector().getClassNameByID(0));
			}
			livingPlayers++;
			p.getInventory().clear();
			p.setExp(0);
			p.setScoreboard(If.getServer().getScoreboardManager()
					.getNewScoreboard());
			classSets.giveClassSets(p);//rnd location, R�stung
		}

		If.getServer().getScheduler()
				.scheduleSyncDelayedTask(If, new Runnable() {

					@Override
					public void run() {
						for (Player p : If.getServer().getOnlinePlayers()) {
							p.setScoreboard(scoreboard.getScoreboard());
							p.setHealth(p.getMaxHealth());
							teleportPlayerToSpawn(p);
						}

					}
				}, 5L);
	}

	/**
	 * Teleports the specified Player to voted map spawn
	 * @param p Specifies the Player
	 */
	private void teleportPlayerToSpawn(Player p) {
		if (map == null)
			System.out.println("Map Object is null");
		p.teleport(map.getSpawnLocation(playerCount));
		playerCount++;
	}
	/////////////Public Methods\\\\\\\\\\\\\\\\\\\\
	/**
	 * Allow Player Flight and makes him invisible to living players
	 * @param p Specifies the dead Player
	 */
	public void setPlayerSpectatorMode(Player p){
		for(Player players : If.getServer().getOnlinePlayers()){
			players.hidePlayer(p);
		}
		p.setAllowFlight(true);
		p.setFlying(true);
	}
	
	public int getCalculatedNextWave(){
		int i = Wave;
		return i + 1;
	}
	/**
	 * Removes an living Player from counter
	 */
	public void removeLivingPlayer(){
		livingPlayers--;
	}
	
	/**
	 * If there are more Monsters then the spawned amount on wave start they will be removed
	 */
	public void removeEntities(){
		for(LivingEntity le : map.getWorld().getLivingEntities()){
			if(le instanceof Player){
				
			}else{
				le.remove();
			}
		}
	}
	public void targetAllCreaturesToOnePlayer(Player p){
		for(Entity e : map.getWorld().getLivingEntities()){
			if(!(e instanceof Player)){
				Creature c = (Creature)e;
				c.setTarget((LivingEntity)p);
			}
		}
	}
	
	public String getMinutesAsString(){
		String s;
		if(minutes < 10){
			s = "0"+minutes;
		}else{
			s = ""+minutes;
		}
		return s;
	}
	public String getSecondsAsString(){
		String s;
		if(seconds < 10){
			s = "0"+seconds;
		}else{
			s = ""+seconds;
		}
		return s;
	}
	public void createTasks() {
		countdownTaskID = If.getServer().getScheduler()
				.scheduleSyncRepeatingTask(If, new Runnable() {

					public void run() {
						if (startCountDown > 0) {
							If.getServer().broadcastMessage(
									If.getChatPrefix() + ChatColor.WHITE
											+ "Runde startet in "
											+ ChatColor.RED + startCountDown
											+ ChatColor.WHITE + " Sekunden !");
							startCountDown--;
						}
						if (startCountDown == 0) {
							If.getServer().broadcastMessage(
									If.getChatPrefix() + ChatColor.WHITE
											+ "Es ist so weit...");
							setWaitForStartGame(false);
							If.getServer().getScheduler()
									.cancelTask(countdownTaskID);
						}
					}
				}, 0L, 20L);
		setMainTaskID(If.getServer().getScheduler()
				.scheduleSyncRepeatingTask(If, new Runnable() {

					@Override
					public void run() {
						if(seconds != -1){
							seconds++;
							if(seconds >= 60){
								minutes++;
								seconds = 0;
							}
							
							scoreboard.setWaveInNameAndTime(Wave, getMinutesAsString(), getSecondsAsString());
						}
						if (waveCountDown > 0 && startCountDown == 0) {
							waveCountDown--;
							for (Player p : If.getServer().getOnlinePlayers()) {
								p.setLevel(waveCountDown);
							}
						}
						if ((waveCountDown < 1) && !(ranTask)) {
							Bukkit.broadcastMessage(If.getChatPrefix()
									+ ChatColor.WHITE + "Welle " + Wave
									+ " beginnt nun !");
							if((Wave) != 1){
								if(RandomChestPositions != null){
									removeRandomChests();
								}
								RandomChestPositions = null;
							}
							if(setWave != -1){
								Wave = setWave;
								setSetWave(-1);
							}

							try {
								if(Wave == 1){
									minutes = 0;
									seconds = 0;
								}
								startWave(Wave);
							} catch (Exception e) {
								e.printStackTrace();
							}
							ranTask = true;
						}
					}
				}, 20L, 20L));
		setTimeChangeTaskID(If.getServer().getScheduler().scheduleSyncRepeatingTask(If, new Runnable() {

			@Override
			public void run() {
				map.getWorld().setTime(40000);
				map.getWorld().strikeLightning(map.getRandomLocation(0));
			}
			
		}, 300L, 300L));
		setWeatherTaskID(If.getServer().getScheduler().scheduleSyncRepeatingTask(If, new Runnable() {
			
			@Override
			public void run() {
				if(randomWeather == 0){
					randomWeather = (int) (Math.random()*7+1);
				}
				if(weatherTaskRuntimes == randomWeather){
					map.getWorld().strikeLightning(map.getRandomLocation(0));
					randomWeather = 0;
				}else{
					randomWeather++;
				}
			}
		}, 60L, 60L));
		setTeleportTaskID(If.getServer().getScheduler().scheduleSyncRepeatingTask(If, new Runnable() {

			@Override
			public void run() {
				for(LivingEntity e : map.getWorld().getLivingEntities()){
					if((e instanceof Player)||(e instanceof Wither)||(e instanceof Enderman)||(e instanceof Ghast)||(e instanceof Blaze)){
						if(isOutOfRange(e.getLocation())){
							e.teleport(map.getRandomLocation(0));
						}
					}
				}
				
			}
			
		}, 200L, 200L));
	}
	
	public void removeRandomChests() {
		Iterator<Location> it = RandomChestPositions.iterator();
		while(it.hasNext()){
			Location l = it.next();
			((Chest)l.getBlock().getState()).getBlockInventory().clear();
			l.getBlock().setType(Material.AIR);
		}
                RandomChestPositions = null;
        }
	
	public void removeEnemieFromCounter(){
		this.setLivingMonsters(this.getLivingMonsters() - 1);
	}
	
	public boolean isOutOfRange(Location l){
		if((int)l.getX() > map.getBorder().MaxX || (int)l.getX() < map.getBorder().MinX){
			return true;
		}else if((int)l.getY() > map.getBorder().MaxEntityY){
			return true;
		}else if((int)l.getZ() > map.getBorder().MaxZ || (int)l.getZ() < map.getBorder().MinZ){
			return true;
		}else{
			return false;
		}
	}
	public void announceGameEnd(int wave){
		for(Player p : If.getServer().getOnlinePlayers()){
			if(!p.getAllowFlight() && Wave == If.getConfigHandler().getMaxWave()){
				If.getGame().getScoreBoard().addPlayerScore(p,"win");
			}
		}
		If.getServer().broadcastMessage(ChatColor.RED+"      Game Over !      ");
		If.getServer().broadcastMessage(If.getChatPrefix()+ChatColor.GREEN+"Ihr habt bis Runde "+ChatColor.WHITE+wave+" �berlebt");
	}
	
	public void resetWaveCountDown() {
		this.waveCountDown = 45;
		this.ranTask = false;
		If.getLogger().info("                    Reset WaveCountDown !");
	}

	
	public void endGameInstance(){
		for(Player p : If.getServer().getOnlinePlayers()){
			p.setAllowFlight(false);
			p.setFlying(false);
		}
		if (RandomChestPositions != null){
			removeRandomChests();
		}
		If.getServer().getScheduler().cancelTask(mainTaskID);
		If.getServer().getScheduler().cancelTask(timeChangeTaskID);
		If.getServer().getScheduler().cancelTask(weatherTaskID);
		gamelistener.unregisterGameListener();
	}
	
	public void spawnRandomChests() {
		RandomChestPositions = new HashSet<Location> ();
		If.getServer().broadcastMessage(If.getChatPrefix()+ChatColor.YELLOW+"Es wurden "+ChatColor.WHITE+If.getConfigHandler().getChestAmount(Wave -1)+ChatColor.YELLOW+" Kisten gespawnt");
		for(int i = 0;i < If.getConfigHandler().getChestAmount(Wave -1);i++){
			Location l = map.getRandomLocation(0);
			l.getBlock().setType(Material.CHEST);
			ItemStack[] is = new ItemStack[2];
			is[0] = new ItemStack(Material.BREAD,If.getConfigHandler().getBread(Wave));
			((Chest)l.getBlock().getState()).getBlockInventory().setContents(is);
			RandomChestPositions.add(l);
		}
	}
	
	public boolean isPlayerDead(Player p){
		if(p.getAllowFlight()){
			return true;
		}else{
			return false;
		}
	}
	
	public void reviveplayer(Player p,String Name){
		p.teleport(new Location(map.getWorld(),-143,18,27));
		p.setScoreboard(If.getGame().getScoreBoard().getScoreboard());
		p.setAllowFlight(false);
		p.setFlying(false);
		p.setGameMode(GameMode.ADVENTURE);
		p.setFoodLevel(20);
		p.setHealth(p.getMaxHealth());
		for(Player onlineP : If.getServer().getOnlinePlayers()){
			onlineP.showPlayer(p);
		}
		p.getInventory().clear();
		classSets.giveClassSets(p);
		livingPlayers++;
		scoreboard.markPlayerAsAlive(p);
		revivedPlayers.add(p);
		PlayerUtilities.sendTitle("�aDu wurdest wiederbelebt.", "",p);
		If.getServer().broadcastMessage(If.getChatPrefix()+ChatColor.DARK_BLUE+"Der Spieler "+ChatColor.GOLD+Name+ChatColor.DARK_BLUE+" wurde wiederbelebt !");
	}
	public void startWave(int Wave) {
		scoreboard.setWaveInNameAndTime(Wave, getMinutesAsString(), getSecondsAsString());
		ConfigHandler c = If.getConfigHandler();
		this.livingMonsters = If.getConfigHandler().getWaveEntityCount(Wave);
		
		for (Player p : If.getServer().getOnlinePlayers()) {
			p.setLevel(livingMonsters);
		}
		map.getWorld().strikeLightningEffect(map.getRandomLocation(0)); //#Special Effects
		// Zombies
		for (int i = 0; i < If.getConfigHandler().getWaveAmount(Wave, "zombie"); i++) {
			Location l = map.getRandomLocation(0);
			map.getWorld().spawnEntity(l, EntityType.ZOMBIE);
		}
		// Creeper
		for (int i = 0; i < c.getWaveAmount(Wave, "creeper"); i++) {
			map.getWorld().spawnEntity(map.getRandomLocation(0), EntityType.CREEPER);
		}
		// Spider
		for (int i = 0; i < c.getWaveAmount(Wave, "spider"); i++) {
			map.getWorld().spawnEntity(map.getRandomLocation(0), EntityType.SPIDER);
		}
		// Skeleton
		for (int i = 0; i < c.getWaveAmount(Wave, "skeleton"); i++) {
			Entity es = map.getWorld().spawnEntity(map.getRandomLocation(0), EntityType.SKELETON);
			Skeleton s = (Skeleton)es;
			s.getEquipment().setItemInHand(new ItemStack(Material.BOW,1));
			s.getEquipment().setItemInHandDropChance(0F);
			s.getEquipment().setHelmetDropChance(0F);
			s.getEquipment().setChestplateDropChance(0F);
			s.getEquipment().setBootsDropChance(0F);
			s.getEquipment().setLeggingsDropChance(0F);
		}
		// Bat
		for (int i = 0; i < c.getWaveAmount(Wave, "bat"); i++) {
			map.getWorld().spawnEntity(map.getRandomLocation(0), EntityType.BAT);
		}
		//loaded Creepers
		for (int i = 0; i < c.getWaveAmount(Wave, "loadedcreeper"); i++) {
			Entity e = map.getWorld().spawnEntity(map.getRandomLocation(0), EntityType.CREEPER);
			Creeper cr = (Creeper)e;
			cr.setPowered(true);
		}
		// Blaze
		for (int i = 0; i < c.getWaveAmount(Wave, "blaze"); i++) {
			map.getWorld().spawnEntity(map.getRandomLocation(1), EntityType.BLAZE);
		}
		// Ghast
		for (int i = 0; i < c.getWaveAmount(Wave, "ghast"); i++) {
			Entity e = map.getWorld().spawnEntity(map.getRandomLocation(3), EntityType.GHAST);
			LivingEntity le = (LivingEntity)e;
			try {
				le.setMaxHealth(30.0);
				le.setHealth(30.0);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		// Wither
		for (int i = 0; i < c.getWaveAmount(Wave, "wither"); i++) {
			map.getWorld().spawnEntity(map.getRandomLocation(3), EntityType.WITHER);
		}
		for(int i = 0; i < c.getWaveAmount(Wave, "cavespider"); i++){
			map.getWorld().spawnEntity(map.getRandomLocation(0), EntityType.CAVE_SPIDER);
		}
		for(int i = 0; i < c.getWaveAmount(Wave, "witch"); i++){
			map.getWorld().spawnEntity(map.getRandomLocation(0), EntityType.WITCH);
		}
	}
	///////////////Getter\\\\\\\\\\\\\\\\\\\\

	/**
	 * @return GameListener Instance
	 */
	public GameListener getGameListener() {
		return this.gamelistener;
	}

	/**
	 * @return InfectionWars Instance
	 */
	public InfectionWars getInfectionWarsInstance() {
		return this.If;
	}

	public Map getMap() {
		return this.map;
	}

	public PlayerScoreboard getScoreBoard() {
		return this.scoreboard;
	}

	public Class_Sets getClassSets() {
		return this.classSets;
	}
	public int getLivingPlayers(){
		return this.livingPlayers;
	}
	public int getPlayerCount(){
		return this.playerCount;
	}
	public int getWave(){
		return this.Wave;
	}

	public int getNextWave() {
		Wave++;
		return Wave;
	}
	
	public int getMainTaskID() {
		return mainTaskID;
	}

	public void setMainTaskID(int mainTaskID) {
		this.mainTaskID = mainTaskID;
	}

	public int getTimeChangeTaskID() {
		return timeChangeTaskID;
	}

	public void setTimeChangeTaskID(int timeChangeTaskID) {
		this.timeChangeTaskID = timeChangeTaskID;
	}

	public int getWeatherTaskID() {
		return weatherTaskID;
	}

	public void setWeatherTaskID(int weatherTaskID) {
		this.weatherTaskID = weatherTaskID;
	}

	public boolean isWaitForStartGame() {
		return waitForStartGame;
	}

	public void setWaitForStartGame(boolean waitForStartGame) {
		this.waitForStartGame = waitForStartGame;
	}

	public int getLivingMonsters() {
		return livingMonsters;
	}
	public boolean getStillWaiting(){
		return this.waitForStartGame;
	}
	public void setLivingMonsters(int livingMonsters) {
		this.livingMonsters = livingMonsters;
	}

	public int getSetWave() {
		return setWave;
	}

	public void setSetWave(int setWave) {
		this.setWave = setWave;
	}

	public int getReviveCostsChips() {
		return reviveCostsChips;
	}

	public void setReviveCostsChips(int reviveCostsChips) {
		this.reviveCostsChips = reviveCostsChips;
	}

	public int getReviveCostsCoins() {
		return reviveCostsCoins;
	}

	public void setReviveCostsCoins(int reviveCostsCoins) {
		this.reviveCostsCoins = reviveCostsCoins;
	}

	public ArrayList<Player> getRevivedPlayers() {
		return revivedPlayers;
	}

	public void setRevivedPlayers(ArrayList<Player> revivedPlayers) {
		this.revivedPlayers = revivedPlayers;
	}

	public int getTeleportTaskID() {
		return teleportTaskID;
	}

	public void setTeleportTaskID(int teleportTaskID) {
		this.teleportTaskID = teleportTaskID;
	}

	public int getMinutes() {
		return minutes;
	}

	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}

	public int getSeconds() {
		return seconds;
	}

	public void setSeconds(int seconds) {
		this.seconds = seconds;
	}

}