package net.pvp_hub.plugin.Infectionwars.game;

import java.io.IOException;

import net.pvp_hub.core.api.PlayerUtilities;
import net.pvp_hub.plugin.Infectionwars.lobby.Lobby;
import net.pvp_hub.plugin.Infectionwars.main.InfectionWars;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Listener registered if there is an running game instance 
 * @author Dubtrackz
 *
 */
public class GameListener implements Listener{
	private InfectionWars If;
	private int EndTaskID;
	/**
	 * GameListener Constructor
	 * @param If InfectionWars current Plugin Instance
	 */
	public GameListener(InfectionWars If){
		this.If = If;
		//Registers GameListener
		this.If.getServer().getPluginManager().registerEvents(this, If);
		this.If.getLogger().info("Registering GameListener .");
	}
	
	///////////EventHandler Section\\\\\\\\\\\\\\\\\
	
	@EventHandler
	public void resetWeather(WeatherChangeEvent e){
		if(e.toWeatherState())e.setCancelled(true);
	}
	
	@EventHandler
	public void onDisconnect(PlayerQuitEvent e){
		e.getPlayer().getInventory().clear();
		e.setQuitMessage(If.getChatPrefix()+ChatColor.RED+"Der Spieler "+ChatColor.WHITE+e.getPlayer().getName()+ChatColor.RED+" hat die Schlacht verlassen !");
		If.getGame().getScoreBoard().markPlayerAsDead(e.getPlayer());
		if((If.getServer().getOnlinePlayers().toArray().length - 1) < 1){//-1 : Player isnt unregisterd yet
			if(EndTaskID != 0){
				If.getServer().getScheduler().cancelTask(EndTaskID);
			}
			If.getGame().endGameInstance();
			If.getLogger().info("Starting new Lobby Instance. Cause : No more Players");
			If.getGame().removeEntities();
			If.setLobby(new Lobby(If,If.getServer().getWorld("InfWarsWorld")));
		}
		int i = 0;
		for(Player p : If.getServer().getOnlinePlayers()){
			if(!p.getAllowFlight()){
				i++;
			}
		}
		if(i-1 == 0){
			end();//FIXME Other End 
		}
	}
	@EventHandler
	public void onBlockSpread(BlockSpreadEvent e){
		e.setCancelled(true);
	}
	@EventHandler
	public void onCreatueSpawn(CreatureSpawnEvent e){
		if(!e.getSpawnReason().equals(SpawnReason.CUSTOM)){
			e.setCancelled(true);
		}
		e.getEntity().setMaximumAir(999999999);
		e.getEntity().setRemainingAir(999999999);
		e.getEntity().setRemoveWhenFarAway(false);
	}
	@EventHandler
	public void respawnEv(PlayerRespawnEvent e){
		e.getPlayer().setScoreboard(If.getGame().getScoreBoard().getScoreboard());
		If.getGame().setPlayerSpectatorMode(e.getPlayer());
		e.setRespawnLocation(If.getGame().getMap().getRandomLocation(0));
	}
	@EventHandler
	public void PlayerDeath(PlayerDeathEvent e){
		Game g = If.getGame();
		e.setDeathMessage(If.getChatPrefix()+ChatColor.WHITE+e.getEntity().getName()+ChatColor.RED+" ist gestorben !");
//		if(eName.length() > 15){
//			eName = eName.substring(0,15);
//		}
		g.getScoreBoard().markPlayerAsDead(e.getEntity());
		e.getDrops().clear();
		g.removeLivingPlayer();
		if(g.getLivingPlayers()<1){
			g.announceGameEnd(If.getGame().getWave());
			end();
		}
	}
	@EventHandler
	public void changeFood(FoodLevelChangeEvent e){
		if(e.getEntity() instanceof Player){
			Player p = (Player)e.getEntity();
			if(p.getAllowFlight()){
				e.setCancelled(true);
			}
		}
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void noSpec(final EntityDamageEvent e){
		if((e.getEntity() instanceof Creeper) && (e.getCause().equals(DamageCause.FIRE) || e.getCause().equals(DamageCause.FIRE_TICK))){
			Creeper c = (Creeper)e.getEntity();
			if(c.isDead()){
				onRoundEnd(If.getGame());
				If.getGame().removeEnemieFromCounter();
			}
		}
		if(e.getEntity() instanceof Player){
			Player p = (Player)e.getEntity();
			if(p.getAllowFlight()){
				e.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void disableCreeperDamage(EntityExplodeEvent e){
		Game g = If.getGame();
		if(e.getEntity() instanceof Creeper){
			e.setCancelled(true);
			g.removeEnemieFromCounter();
			onRoundEnd(g);
		}
		if(e.getEntity() instanceof Fireball){
			e.setCancelled(true);
			((Fireball)e.getEntity()).setBounce(false);
			((Fireball)e.getEntity()).setYield(0F);
		}
	}
	@EventHandler
	public void NoDrop(PlayerDropItemEvent e){
		if((!e.getPlayer().isOp()) || (!(e.getItemDrop().getItemStack().getType() == Material.BREAD))){
			e.setCancelled(true);
		}
	}
	@EventHandler
	public void GameStartWait(PlayerMoveEvent e){
		if(If.getGame().getStillWaiting()){
			if(!(LocationEqualsXYZ(e.getFrom(), e.getTo()))){
				e.getPlayer().teleport(e.getFrom());
			}
		}
	}
	
	@EventHandler
	public void preventEntityfromSpectatorDamage(EntityDamageByEntityEvent e){
		if(!(e.getEntity() instanceof Player)){
			if(e.getDamager() instanceof Player){
				final Player p = (Player)e.getDamager();
				resetArmor(p);
				if(!p.getItemInHand().getType().equals(Material.POTION)){
	                            p.getItemInHand().setDurability((short)0);
	                        }
				if(p.getAllowFlight()){
					e.setCancelled(true);
				}
			}
		}else{
			Player p = (Player)e.getEntity();
			if(p.getAllowFlight()){
				e.setCancelled(true);
			}
			if(!p.getItemInHand().getType().equals(Material.POTION)){
	                          p.getItemInHand().setDurability((short)0);
	                }
		}
	}
	
	@EventHandler
	public void EntityTarget(EntityTargetEvent e){
		if(e.getTarget() instanceof Player){
				Player p = (Player)e.getTarget();
				if(p.getAllowFlight()){
					e.setCancelled(true);
				}
		}
		
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void preventBowDamage(final EntityShootBowEvent e){
		if(e.getEntity() instanceof Player){
			If.getServer().getScheduler().scheduleSyncDelayedTask(If, new Runnable() {
				
				@Override
				public void run() {
					e.getBow().setDurability((short)0);
				}
			}, 2L);
		}
	}
	
	
	@EventHandler
	public void removeEmptyBottles(final PlayerItemConsumeEvent e){
	    if (e.getItem().getData().getItemType().equals(Material.POTION)) { 
	    	If.getServer().getScheduler().scheduleSyncDelayedTask(If, new Runnable(){ 
	            public void run() { 
	            	e.getPlayer().getInventory().setItem(e.getPlayer().getInventory().first(new ItemStack(Material.GLASS_BOTTLE)), new ItemStack(Material.AIR));
	            } 
	        }, 2L);

	    }
	}
	
	
	/**
	 * Don't allow Players to join later
	 */
	@EventHandler
	public void onJoin(PlayerLoginEvent e){
		e.setResult(Result.KICK_OTHER);
		e.getPlayer().kickPlayer("Die Runde hat bereits begonnen ! Bitte suche dir einen anderen Server !");
	}
	
	@EventHandler
	public void controlBuild(BlockBreakEvent e){
		if(!e.getPlayer().isOp()){
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void disableSpectatorLoot(PlayerInteractEvent e){
		if(e.getClickedBlock() == null || e.getClickedBlock().getType() == Material.AIR){
			return;
		}
		if(e.getPlayer().getAllowFlight() && e.getClickedBlock().getType() == Material.CHEST){
			e.setCancelled(true);
		}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onEntityDeath(EntityDeathEvent e){
		e.setDroppedExp(0);
		e.getDrops().clear();
		Game g = If.getGame();
		g.removeEnemieFromCounter();
		Player killer = e.getEntity().getKiller();
		if(killer == null)return;
		if(!(e.getEntity() instanceof Player)){
			Player k = (Player)e.getEntity().getKiller();
			g.getScoreBoard().addPlayerScore(k, e.getEntityType().getName().toLowerCase());
		}
		onRoundEnd(g);
	}
	/////////Methodes section \\\\\\\\\\\\
	public void resetArmor(Player p){
//		If.getServer().getScheduler().scheduleSyncDelayedTask(If, new Runnable(){
//
//			@Override
//			public void run() {
//				for(ItemStack item : p.getInventory().getArmorContents()){
//					if(item != null){
//						item.setDurability((short)0);
//					}
//				}
//				
//			}
//			
//		}, 2L);
	}
	
	public boolean LocationEqualsXYZ(Location from,Location to){
		if(from.getX() == to.getX() && from.getY() == to.getY() && from.getZ() == to.getZ()){
			return true;
		}else{
			return false;
	}
	}
	
	public void end(){
		EndTaskID = If.getServer().getScheduler().scheduleSyncDelayedTask(If, new Runnable(){

			@Override
			public void run() {
				If.getGame().removeEntities();
				If.getGame().endGameInstance();
				for(Player all : If.getServer().getOnlinePlayers()){
					  try {
						PlayerUtilities.connect(If, "lobby", all);
						If.getServer().reload();
					  } catch (IOException e) {
						e.printStackTrace();
					}
					}
				If.setLobby(new net.pvp_hub.plugin.Infectionwars.lobby.Lobby(If,If.getServer().getWorld("InfWarsWorld")));
			}
			
		}, 400L);
	}
	
	public void onRoundEnd(Game g){
		if(g.getLivingMonsters()< 1){
			if(If.getConfigHandler().getMaxWave() == g.getWave()){
				If.getLogger().info("Max Wave : "+If.getConfigHandler().getMaxWave()+"  - current Wave : "+g.getWave());
				g.announceGameEnd(g.getWave());
				end();
				return;
			}
			If.getServer().broadcastMessage(If.getChatPrefix()+ChatColor.GREEN+"Ihr habt Welle "+g.getWave()+" �berlebt !");
			If.getServer().broadcastMessage(If.getChatPrefix()+ChatColor.RED+"Macht euch bereit f�r Welle "+ChatColor.WHITE+ g.getNextWave());
			g.resetWaveCountDown();
			g.spawnRandomChests();
			for(Entity e : If.getGame().getMap().getWorld().getEntities()){
				if(!(e instanceof Player)){
					e.remove();//LoL
				}
			}
		}
		if(g.getLivingMonsters()>0){
			for (Player p : If.getServer().getOnlinePlayers()) {
				p.setLevel(If.getGame().getLivingMonsters());
			}
		}
	}
	
	/**
	 * @return Current InfectionWars Plugin Instance
	 */
	public InfectionWars getInfectionWarsInstance(){
		return this.If;
	}
	
	/**
	 * Unregisters GameListerner
	 */
	public void unregisterGameListener(){
		this.If.getServer().getLogger().info("Unregistering GameListener");
		HandlerList.unregisterAll(this);
	}
}
