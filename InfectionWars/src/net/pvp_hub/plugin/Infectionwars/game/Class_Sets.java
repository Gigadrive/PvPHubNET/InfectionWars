package net.pvp_hub.plugin.Infectionwars.game;

import net.pvp_hub.plugin.Infectionwars.general.Class;
import net.pvp_hub.plugin.Infectionwars.main.InfectionWars;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Creates ItemStacks with Class Items
 * @author Dubtrackz
 *
 */
public class Class_Sets {
	private ItemStack[] Chemist;
	private ItemStack[] ChemistAmor;
	private ItemStack[] Archer;
	private ItemStack[] ArcherAmor;
	private ItemStack[] Knight;
	private ItemStack[] KnightAmor;
	private ItemStack[] Heavy;
	private ItemStack[] HeavyAmor;
	private ItemStack[] AllRound;
	private ItemStack[] AllRoundAmor;
	private ItemStack[] Fighter;
	private ItemStack[] Statue;
	private ItemStack[] StatueAmor;
	private ItemStack[] Survivor;
	private ItemStack[] SurvivorAmor;
	private ItemStack[] Vips;
	private ItemStack[] VipsAmor;
	private InfectionWars If;
	public Class_Sets(InfectionWars If){
		// Classes
		// ChemistAmor
		this.If = If;
		ItemStack i;
		Chemist = new ItemStack[8];
		Chemist[0] = new ItemStack(Material.GOLD_SWORD,1);
		Chemist[1] = new ItemStack(Material.POTION,30,(short)16453);
		Chemist[2] = new ItemStack(Material.POTION,20,(short)16421);
		Chemist[3] = new ItemStack(Material.POTION,30,(short)16460);
		Chemist[4] = new ItemStack(Material.POTION,20,(short)16428);
		Chemist[5] = new ItemStack(Material.POTION,5,(short)16452);
		Chemist[6] = new ItemStack(Material.POTION,2,(short)16420);
		Chemist[7] = new ItemStack(Material.POTION,5,(short)16418);
		ChemistAmor = new ItemStack[4];
		ChemistAmor[3] = new ItemStack(Material.GOLD_HELMET,1);
		ChemistAmor[2] = new ItemStack(Material.GOLD_CHESTPLATE,1);
		ChemistAmor[1] = new ItemStack(Material.GOLD_LEGGINGS,1);
		ChemistAmor[0] = new ItemStack(Material.GOLD_BOOTS,1);
		// Archer
		Archer = new ItemStack[5];
		Archer[0] = new ItemStack(Material.WOOD_SWORD,1);
		i = new ItemStack(Material.BOW);
		i.addEnchantment(Enchantment.ARROW_INFINITE, 1);
		i.addEnchantment(Enchantment.ARROW_DAMAGE, 4);
		i.addEnchantment(Enchantment.ARROW_FIRE, 1);
		Archer[1] = i;
		Archer[2] = new ItemStack(Material.POTION,10,(short)8226);
		Archer[3] = new ItemStack(Material.POTION,20,(short)8261);
		Archer[4] = new ItemStack(Material.ARROW,1);
		ArcherAmor = new ItemStack[4];
		ArcherAmor[3] = new ItemStack(Material.CHAINMAIL_HELMET,1);
		ArcherAmor[2] = new ItemStack(Material.CHAINMAIL_CHESTPLATE,1);
		ArcherAmor[1] = new ItemStack(Material.CHAINMAIL_LEGGINGS,1);
		ArcherAmor[0] = new ItemStack(Material.CHAINMAIL_BOOTS,1);
		//Knight
		Knight = new ItemStack[4];
		i = new ItemStack(Material.DIAMOND_SWORD,1);
		i.addEnchantment(Enchantment.DAMAGE_ALL, 4);
		Knight[0] = i;
		Knight[1] = new ItemStack(Material.POTION,3,(short)8265);
		Knight[2] = new ItemStack(Material.POTION,1,(short)8233);
		Knight[3] = new ItemStack(Material.POTION,20,(short)8261);
		KnightAmor = new ItemStack[4];
		KnightAmor[3] = new ItemStack(Material.IRON_HELMET,1);
		KnightAmor[2] = new ItemStack(Material.IRON_CHESTPLATE,1);
		KnightAmor[1] = new ItemStack(Material.IRON_LEGGINGS,1);
		KnightAmor[0] = new ItemStack(Material.IRON_BOOTS,1);
		//Heavy
		Heavy = new ItemStack[4];
		i = new ItemStack(Material.IRON_SWORD,1);
		i.addEnchantment(Enchantment.DAMAGE_ALL, 1);
		Heavy[0] = i;
		Heavy[1] = new ItemStack(Material.POTION,1,(short)8265);
		Heavy[2] = new ItemStack(Material.POTION,30,(short)8261);
		Heavy[3] = new ItemStack(Material.POTION,20,(short)8229);
		HeavyAmor = new ItemStack[4];
		i = new ItemStack(Material.DIAMOND_HELMET,1);i.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		HeavyAmor[3] = i;
		i = new ItemStack(Material.DIAMOND_CHESTPLATE,1);i.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		HeavyAmor[2] = i;
		i = new ItemStack(Material.DIAMOND_LEGGINGS,1);i.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		HeavyAmor[1] = i;
		i = new ItemStack(Material.DIAMOND_BOOTS,1);i.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 1);
		HeavyAmor[0] = i;
		//Allround
		AllRound = new ItemStack[8];
		i = new ItemStack(Material.IRON_SWORD,1);i.addEnchantment(Enchantment.DAMAGE_UNDEAD, 1);
		AllRound[0] = i;
		i = new ItemStack(Material.BOW,1);i.addEnchantment(Enchantment.ARROW_INFINITE, 1);
		AllRound[1] = i;
		AllRound[2] = new ItemStack(Material.ARROW,1);
		AllRound[3] = new ItemStack(Material.POTION,20,(short)8261);
		AllRound[4] = new ItemStack(Material.POTION,5,(short)8229);
		AllRound[5] = new ItemStack(Material.POTION,3,(short)16420);
		AllRound[6] = new ItemStack(Material.POTION,10,(short)16460);
		AllRound[7] = new ItemStack(Material.POTION,3,(short)16428);
		AllRoundAmor = new ItemStack[4];
		AllRoundAmor[3] = new ItemStack(Material.DIAMOND_HELMET,1);
		AllRoundAmor[2] = new ItemStack(Material.LEATHER_CHESTPLATE,1);
		AllRoundAmor[1] = new ItemStack(Material.IRON_LEGGINGS,1);
		AllRoundAmor[0] = new ItemStack(Material.GOLD_BOOTS,1);
		//Kämpfer
		Fighter = new ItemStack[2];
		i = new ItemStack(Material.DIAMOND_SWORD,1);i.addEnchantment(Enchantment.FIRE_ASPECT,1);i.addEnchantment(Enchantment.DAMAGE_ALL, 5);
		Fighter[0] = i;
		Fighter[1] = new ItemStack(Material.POTION,5,(short)8233);
		//////#NoAmor
		//statue
		Statue = new ItemStack[2];
		Statue[0] = new ItemStack(Material.WOOD_SWORD,1);
		Statue[1] = new ItemStack(Material.POTION,50,(short)8261);
		StatueAmor = new ItemStack[4];
		i = new ItemStack(Material.DIAMOND_HELMET);i.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
		StatueAmor[3] = i;
		i = new ItemStack(Material.DIAMOND_CHESTPLATE);i.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
		StatueAmor[2] = i;
		i = new ItemStack(Material.DIAMOND_LEGGINGS);i.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
		StatueAmor[1] = i;
		i = new ItemStack(Material.DIAMOND_BOOTS);i.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 3);
		StatueAmor[0] = i;
		//Survivor
		Survivor = new ItemStack[4];
		i = new ItemStack(Material.WOOD_SWORD,1);i.addEnchantment(Enchantment.DAMAGE_ALL, 5);i.addEnchantment(Enchantment.FIRE_ASPECT, 1);
		Survivor[0] = i;
		Survivor[1] = new ItemStack(Material.FISHING_ROD,1);
		Survivor[2] = new ItemStack(Material.CARROT_STICK,1);
		Survivor[3] = new ItemStack(Material.POTION,19,(short)8261);
		SurvivorAmor = new ItemStack[4];
		SurvivorAmor[3] = new ItemStack(Material.LEATHER_HELMET,1);
		SurvivorAmor[2] = new ItemStack(Material.LEATHER_CHESTPLATE,1);
		SurvivorAmor[1] = new ItemStack(Material.LEATHER_LEGGINGS,1);
		SurvivorAmor[0] = new ItemStack(Material.LEATHER_BOOTS,1);
		//Vips
		Vips = new ItemStack[6];
		i = new ItemStack(Material.DIAMOND_SWORD,1);i.addEnchantment(Enchantment.DAMAGE_ALL, 5);i.addEnchantment(Enchantment.FIRE_ASPECT, 2);
		Vips[0] = i;
		Vips[1] = new ItemStack(Material.POTION,60,(short)8261);
		Vips[2] = new ItemStack(Material.POTION,60,(short)8229);
		Vips[3] = new ItemStack(Material.POTION,60,(short)8268);
		Vips[4] = new ItemStack(Material.POTION,60,(short)8236);
		Vips[5] = new ItemStack(Material.POTION,60,(short)8233);
		VipsAmor = new ItemStack[4];
		i = new ItemStack(Material.DIAMOND_HELMET,1);i.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
		VipsAmor[3] = i;
		i = new ItemStack(Material.DIAMOND_CHESTPLATE,1);i.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
		VipsAmor[2] = i;
		i = new ItemStack(Material.DIAMOND_LEGGINGS,1);i.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
		VipsAmor[1] = i;
		i = new ItemStack(Material.DIAMOND_BOOTS,1);i.addEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 4);
		VipsAmor[0] = i;
	}
	public void giveClassSets(Player p) {
		if (If.getLobby().getClassSelector().getPlayerClass(p) == Class.CHEMIST) {
			p.getInventory().setContents(Chemist);
			p.getInventory().setArmorContents(ChemistAmor);
		}else if(If.getLobby().getClassSelector().getPlayerClass(p) == Class.ARCHER){
			p.getInventory().setContents(Archer);
			p.getInventory().setArmorContents(ArcherAmor);
		}else if(If.getLobby().getClassSelector().getPlayerClass(p) == Class.KNIGHT){
			p.getInventory().setContents(Knight);
			p.getInventory().setArmorContents(KnightAmor);
		}else if(If.getLobby().getClassSelector().getPlayerClass(p) == Class.HEAVY){
			p.getInventory().setContents(Heavy);
			p.getInventory().setArmorContents(HeavyAmor);
		}else if(If.getLobby().getClassSelector().getPlayerClass(p) == Class.ALLROUND){
			p.getInventory().setContents(AllRound);
			p.getInventory().setArmorContents(AllRoundAmor);
		}else if(If.getLobby().getClassSelector().getPlayerClass(p) == Class.FIGHTER){
			p.getInventory().setContents(Fighter);
		}else if(If.getLobby().getClassSelector().getPlayerClass(p) == Class.STATUE){
			p.getInventory().setContents(Statue);
			p.getInventory().setArmorContents(StatueAmor);
		}else if(If.getLobby().getClassSelector().getPlayerClass(p) == Class.SURVIVOR){
			p.getInventory().setContents(Survivor);
			p.getInventory().setArmorContents(SurvivorAmor);
		}else if(If.getLobby().getClassSelector().getPlayerClass(p) == Class.VIPS){
			p.getInventory().setContents(Vips);
			p.getInventory().setArmorContents(VipsAmor);
		}
		
	}
	/////////////Getter\\\\\\\\\\\\\\\\\\\\\\\
	public ItemStack[] getChemist(){
		return this.Chemist;
	}
	public ItemStack[] getChemistAmor(){
		return this.ChemistAmor;
	}
	public ItemStack[] getArcher(){
		return this.Archer;
	}
	public ItemStack[] getArcherAmor(){
		return this.ArcherAmor;
	}
	public ItemStack[] getKnight(){
		return this.Knight;
	}
	public ItemStack[] getKnightAmor(){
		return this.KnightAmor;
	}
	public ItemStack[] getHeavy(){
		return this.Heavy;
	}
	public ItemStack[] getHeavyAmor(){
		return this.HeavyAmor;
	}
	public ItemStack[] getAllRound(){
		return this.AllRound;
	}
	public ItemStack[] getAllRoundAmor(){
		return this.AllRoundAmor;
	}
	public ItemStack[] getFighter(){
		return this.Fighter;
	}
	public ItemStack[] getStatue(){
		return this.Statue;
	}
	public ItemStack[] getStatueAmor(){
		return this.StatueAmor;
	}
	public ItemStack[] getSurvivor(){
		return this.Survivor;
	}
	public ItemStack[] getSurvivorAmor(){
		return this.SurvivorAmor;
	}
	public ItemStack[] getVips(){
		return this.Vips;
	}
	public ItemStack[] getVipsAmor(){
		return this.VipsAmor;
	}
}